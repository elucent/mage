# Mage

Mage is a simple game framework for C++ using the OpenGL and OpenAL libraries.
It's heavily WIP, so expect more examples, tutorials, and documentation in the
future. For now, feel free to look through the code!