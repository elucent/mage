#include "Definitions.h"
#include "Testing/Testing.h"
#include "Math/Vectors.h"

using namespace std;
using namespace mage;

TEST(vector_default_ctor) {
    Vec<5> v;
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(v[i], 0);
    }
}

TEST(vector_element_ctor) {
    Vec<5> v(7);
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(v[i], 7);
    }
}

TEST(vector_list_ctor) {
    Vec<8> v = { 1, 1, 2, 3, 5, 8, 13, 21 };

    int a = 1, b = 1, c = 2;
    for (int i = 0; i < 8; i ++) {
        ASSERT_ALMOST_EQUAL(a, v[i]);
        a = b;
        b = c;
        c = a + b;
    }

    try {
        Vec<8> v2 = { 1, 2, 3 };
    }
    catch (DataError& e) {
        return;
    }
    ASSERT_TRUE(false);
}

TEST(vector_copy_ctor) {
    Vec<5> v = { 1, 2, 3, 4, 5 };
    Vec<5> c(v);
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(c[i], i + 1);
    }
    v[0] = 4;
    v[4] = 6;
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(c[i], i + 1);
    }
}

TEST(vector_assignment) {
    Vec<5> v = { 1, 2, 3, 4, 5 };
    Vec<5> a;
    a = v;
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(a[i], i + 1);
    }
    v[0] = 4;
    v[4] = 6;
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(a[i], i + 1);
    }

    ASSERT_EQUAL(&v, &(v = v));
}

TEST(vector_size) {
    Vec2 v2 = { 1, 2 };
    Vec3 v3 = { 1, 2, 3 };
    Vec4 v4 = { 1, 2, 3, 4 };
    ASSERT_EQUAL(v2.Size(), 2);
    ASSERT_EQUAL(v3.Size(), 3);
    ASSERT_EQUAL(v4.Size(), 4);

    Vec<7> v;
    ASSERT_EQUAL(v.Size(), 7);
}

TEST(vector_mag) {
    Vec2 a = { 3, 4 };
    ASSERT_ALMOST_EQUAL(a.Dist(), 5);

    Vec4 b = { 1, 0, 0, 0 };
    ASSERT_ALMOST_EQUAL(b.Dist(), 1);
}

TEST(vector_sqmag) {
    Vec2 a = { 3, 4 };
    ASSERT_ALMOST_EQUAL(a.DistSq(), 25);

    Vec4 b = { 1, 0, 0, 0 };
    ASSERT_ALMOST_EQUAL(b.DistSq(), 1);
}

TEST(vector_normal) {
    Vec3 a = { 0, 1, 0 };
    Vec3 b = { 3, 4, 0 };
    Vec3 c = { .6, .8, 0 };

    ASSERT_TRUE(a.IsNormal());
    ASSERT_FALSE(b.IsNormal());
    ASSERT_TRUE(c.IsNormal());
}

TEST(vector_normalize) {
    Vec3 a = { 0, 1, 0 };
    Vec3 b = { 3, 4, 0 };
    Vec3 c = { .6, .8, 0 };

    ASSERT_EQUAL(b.Normalize(), c);
    ASSERT_EQUAL(a.Normalize(), a);
}

TEST(vector_dot) {
    Vec3 a = { 0, 1, 0 };
    Vec3 b = { 1, 0, 0 };
    Vec3 c = { 1, 3, 0 };
    Vec3 d = { 2, 4, 0 };

    ASSERT_EQUAL(Dot(a, b), 0);
    ASSERT_EQUAL(Dot(c, d), 14);
}

TEST(vector_cross) {
    Vec3 a = { 1, 0, 0 };
    Vec3 b = { 0, 1, 0 };
    Vec3 c = { 0, 0, 1 };

    ASSERT_EQUAL(Cross(a, b), c);

    Vec4 d = { 1, 2, 3, 4 };
    Vec4 e = { 4, 3, 2, 1 };
    try {
        auto f = Cross(d, e);
    }
    catch (ArithmeticError& e) {
        return;
    }
    ASSERT_TRUE(false);
}

TEST(vector_addition) { 
    Vec<5> a = { 1, 2, 3, 4, 5 };
    Vec<5> b = { 2.5, 2, 1.5, 1, 0.5 };
    auto c = a + b;
    a += b;
    ASSERT_EQUAL(c.Size(), 5);
    ASSERT_EQUAL(a, c);

    Vec<5> result = { 3.5, 4, 4.5, 5, 5.5 };
    ASSERT_EQUAL(c, result);
}

TEST(vector_subtraction) { 
    Vec3 a = { 4, 5, 2 };
    Vec3 b = { 2, 3, 0 };
    auto c = a - b;
    a -= b;
    ASSERT_EQUAL(c.Size(), 3);
    ASSERT_EQUAL(a, c);

    Vec3 result = { 2, 2, 2 };
    ASSERT_EQUAL(c, result);
}

TEST(vector_multiplication) { 
    Vec4 a = { 2, 4, 3, 1 };
    Vec4 b = { 6, 4.5, 5, 2 };
    auto c = a * b;
    a *= b;
    ASSERT_EQUAL(c.Size(), 4);
    ASSERT_EQUAL(a, c);

    Vec4 result = { 12, 18, 15, 2 };
    ASSERT_EQUAL(c, result);
}

TEST(vector_division) { 
    Vec3 a = { 51, 21, 17.5 };
    Vec3 b = { 34, 3, 2.5 };
    auto c = a / b;
    a /= b;
    ASSERT_EQUAL(c.Size(), 3);
    ASSERT_EQUAL(a, c);

    Vec3 result = { 1.5, 7, 7 };
    ASSERT_EQUAL(c, result);
}

TEST(vector_modulus) { 
    Vec3 a = { 51, 21.4, 1.568 };
    Vec3 b = { 34, 3, 1.5 };
    auto c = a % b;
    a %= b;
    ASSERT_EQUAL(c.Size(), 3);
    ASSERT_EQUAL(a, c);

    Vec3 result = { 17, .4, .068 };
    ASSERT_EQUAL(c, result);
}

TEST(vector_negation) {
    Vec4 a = { 1, 2.4, 3.6, 0 };
    Vec4 b = { -1, -2.4, -3.6, 0 };
    ASSERT_EQUAL(-a, b);
    ASSERT_EQUAL(a, -b);
}

TEST(vector_equality) {
    Vec3 a = { 1, 2, 3 };
    Vec3 b = { 1, 2, 3 };
    ASSERT_TRUE(a == b);
    ASSERT_FALSE(a != b);
    
    Vec3 c = { 2, 4, 5 };
    ASSERT_FALSE(a == c);
    ASSERT_TRUE(a != c);

    Vec3 d = { 1, 2, 4 };
    ASSERT_FALSE(b == d);
    ASSERT_TRUE(a != d);
}

TEST(vector_concat) {
    Vec3 a = { 1, 2, 3 };
    Vec2 b = { 4, 5 };
    auto c = a & b;
    ASSERT_EQUAL(c.Size(), 5);
    for (int i = 0; i < 5; i ++) {
        ASSERT_ALMOST_EQUAL(c[i], i + 1);
    }
}

TEST(vector_append) {
    Vec2 a = { 0, 1 };
    auto b = a & 2 & 3;
    ASSERT_EQUAL(b.Size(), 4);
    ASSERT_EQUAL((b & 5).Size(), 5);
    for (int i = 0; i < 4; i ++) {
        ASSERT_ALMOST_EQUAL(b[i], i);
    }
}

TEST(vector_output) {
    ostringstream os;
    Vec3 v = { 4, 5, 6 };
    os << v;
    ASSERT_EQUAL("{ 4 5 6 }", os.str());
}

TEST(vector_input) {
    Vec3 v;
    istringstream is("1 2 3");
    is >> v;
    ASSERT_EQUAL(v, (Vec3{1, 2, 3}));
}

RUN();