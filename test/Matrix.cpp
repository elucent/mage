#include "Testing/Testing.h"
#include "Math/Matrix.h"

using namespace std;
using namespace mage;

TEST(matrix_default_ctor) {
    Mat<4, 3> m;
    for (int i = 0; i < 4; i ++) {
        for (int j = 0; j < 3; j ++) {
            ASSERT_ALMOST_EQUAL(m(i, j), 0);
        }
    }
}

TEST(matrix_element_ctor) {
    Mat<3, 4> m(1);
    for (int i = 0; i < 4; i ++) {
        for (int j = 0; j < 3; j ++) {
            ASSERT_ALMOST_EQUAL(m(i, j), 1);
        }
    }
}

TEST(matrix_list_ctor) {
    Mat<3, 4> m = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    Mat<3, 4> ref;
    ref(0, 0) = 1;
    ref(0, 1) = 2;
    ref(0, 2) = 3;
    ref(0, 3) = 4;
    ref(1, 0) = 5;
    ref(1, 1) = 6;
    ref(1, 2) = 7;
    ref(1, 3) = 8;
    ref(2, 0) = 9;
    ref(2, 1) = 10;
    ref(2, 2) = 11;
    ref(2, 3) = 12;
    ASSERT_EQUAL(m, ref);

    Mat<1, 1> box = { 1 };
    ASSERT_EQUAL(m(0, 0), 1);
}

TEST(matrix_list_ctor_wrong_size) {
    try {
        Mat<3, 4> m = { 1, 2, 3, 4, 5, 6, 7 };
    }
    catch (DataError& e) {
        return;
    }
    FAIL("Did not catch incorrectly sized initializer.");
}

TEST(matrix_grid_ctor) {
    Mat<3, 4> m = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
    Mat<3, 4> ref;
    ref(0, 0) = 1;
    ref(0, 1) = 2;
    ref(0, 2) = 3;
    ref(0, 3) = 4;
    ref(1, 0) = 5;
    ref(1, 1) = 6;
    ref(1, 2) = 7;
    ref(1, 3) = 8;
    ref(2, 0) = 9;
    ref(2, 1) = 10;
    ref(2, 2) = 11;
    ref(2, 3) = 12;
    ASSERT_EQUAL(m, ref);
}

TEST(matrix_grid_ctor_wrong_size) {
    try {
        Mat<3, 4> m = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
    }
    catch (DataError& e) {
        return;
    }
    FAIL("Did not catch incorrectly sized initializer.");
}

TEST(matrix_grid_ctor_uneven) {
    try {
        Mat<3, 4> m = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11 } };
    }
    catch (DataError& e) {
        return;
    }
    FAIL("Did not catch unevenly sized initializer.");
}

TEST(matrix_vectors_ctor) {
    Vec4 a = { 1, 2, 3, 4 }, b = { 5, 6, 7, 8 }, c = { 9, 10, 11, 12 };
    Mat<3, 4> m = { a, b, c };
    Mat<3, 4> ref;
    ref(0, 0) = 1;
    ref(0, 1) = 2;
    ref(0, 2) = 3;
    ref(0, 3) = 4;
    ref(1, 0) = 5;
    ref(1, 1) = 6;
    ref(1, 2) = 7;
    ref(1, 3) = 8;
    ref(2, 0) = 9;
    ref(2, 1) = 10;
    ref(2, 2) = 11;
    ref(2, 3) = 12;
    ASSERT_EQUAL(m, ref);
}

TEST(matrix_vectors_ctor_wrong_size) {
    try {
        Mat<3, 4> m = { Vec4{ 1, 2, 3, 4 }, Vec4{ 5, 6, 7, 8 } };
    }
    catch (DataError& e) {
        return;
    }
    FAIL("Did not catch incorrectly sized initializer.");
}

TEST(matrix_width) {
    Mat<5, 7> m;
    Mat4 m4;
    ASSERT_EQUAL(m.Width(), 7);
    ASSERT_EQUAL(m4.Width(), 4);
}

TEST(matrix_height) {
    Mat<5, 7> m;
    Mat4 m4;
    ASSERT_EQUAL(m.Height(), 5);
    ASSERT_EQUAL(m4.Height(), 4);
}

TEST(matrix_columns) {
    Mat<5, 7> m;
    Mat4 m4;
    ASSERT_EQUAL(m.Columns(), 7);
    ASSERT_EQUAL(m4.Columns(), 4);
}

TEST(matrix_rows) {
    Mat<5, 7> m;
    Mat4 m4;
    ASSERT_EQUAL(m.Rows(), 5);
    ASSERT_EQUAL(m4.Rows(), 4);
}

TEST(matrix_transpose) {
    Mat<4, 4> m;
    ASSERT_EQUAL(m, m.Transpose());

    Mat<2, 3> m2 = { 1, 2, 3, 4, 5, 6 };
    Mat<3, 2> ref = { { 1, 4 }, { 2, 5 }, { 3, 6 } };
    ASSERT_EQUAL(m2.Transpose(), ref);
}

TEST(matrix_addition) {
    Mat<2, 3> a = { { 1, 2, 3 }, { 4, 5, 6 } };
    Mat<2, 3> b = { { 9, 8, 7 }, { 6, 5, 4 } };
    Mat<2, 3> c(10);
    ASSERT_EQUAL(a + b, c);
}

TEST(matrix_subtraction) {
    Mat<2, 3> a = { { 1, 2, 3 }, { 4, 5, 6 } };
    Mat<2, 3> b = { { 1, 2, 3 }, { 4, 5, 6 } };
    Mat<2, 3> c;
    ASSERT_EQUAL(a - b, c);
}

TEST(matrix_negation) {
    Mat<2, 3> a = { { 1, 2, 3 }, { 4, 5, 6 } };
    Mat<2, 3> b = { { -1, -2, -3 }, { -4, -5, -6 } };
    ASSERT_EQUAL(-a, b);
    ASSERT_EQUAL(a, -b);
}

TEST(matrix_access) {
    Mat<2, 3> a = { { 1, 2, 3 }, { 4, 5, 6 } };
    a(0, 0) = 4;
    a(1, 1) = 0;
    Mat<2, 3> refa = { { 4, 2, 3 }, { 4, 0, 6 } };
    ASSERT_EQUAL(a, refa);
}

TEST(matrix_equality) {
    Mat<2, 3> a = { 1, 2, 3, 4, 5, 6 };
    Mat<2, 3> b = { { 1, 2, 3 }, { 4, 5, 6 } };
    ASSERT_EQUAL(a, b);
    Mat<2, 3> c = { 2, 2, 3, 4, 5, 6 };
    ASSERT_FALSE(a == c);
}

TEST(matrix_inequality) {
    Mat<2, 3> a = { 1, 2, 3, 4, 5, 6 };
    Mat<2, 3> b = { { 1, 2, 3 }, { 4, 5, 6 } };
    ASSERT_FALSE(a != b);
    Mat<2, 3> c = { 2, 2, 3, 4, 5, 6 };
    ASSERT_TRUE(a != c);
}

RUN();