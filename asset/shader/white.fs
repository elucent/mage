#version 330

in vec4 vsh_color;
in vec4 vsh_sprite;
in vec2 vsh_texcoord;

out vec4 fsh_color;

uniform sampler2D tex;

void main() {
    vec2 uv = vsh_sprite.xy;
    uv += fract(vsh_texcoord) * vsh_sprite.zw;
    fsh_color = vsh_color * texture(tex, uv);
    if (fsh_color.a < 0.05f) discard;
    fsh_color = vec4(1, 1, 1, 1);
}