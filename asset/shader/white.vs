#version 330
layout(location = 0) in vec3 MAGE_position;
layout(location = 1) in vec4 MAGE_color;
layout(location = 2) in vec3 MAGE_normal;
layout(location = 3) in vec2 MAGE_texcoord;
layout(location = 4) in vec4 MAGE_sprite;

out vec4 vsh_color;
out vec4 vsh_sprite;
out vec2 vsh_texcoord;

uniform mat4 MAGE_projection, MAGE_camera, MAGE_transform;
uniform int MAGE_texwidth, MAGE_texheight;

void main() {
    gl_Position = MAGE_projection * MAGE_camera * MAGE_transform * vec4(MAGE_position, 1);

    vec3 normal = mat3(MAGE_transform) * MAGE_normal;

    float light = 1.1f * (dot(normal, normalize(vec3(0.3f, -1, 0.65f))) + 2.0f) / 3.0f;
    if (normal.x == 0 && normal.y == 0 && normal.z == 0) light = 1.0f;
    vsh_color = vec4(light * MAGE_color.xyz, MAGE_color.w);
    vsh_texcoord = MAGE_texcoord;
    vec2 texdims = vec2(1.0f / MAGE_texwidth, 1.0f / MAGE_texheight);
    vsh_sprite = MAGE_sprite * texdims.xyxy;
}