#version 330

in vec2 vsh_texcoord;

out vec4 fsh_color;

uniform int MAGE_viewwidth, MAGE_viewheight;
uniform sampler2D fb;

const float PI = 3.1415926535897932384626433832795;

vec4 at(vec2 uv, float dx, float dy) {
    return texture(fb, clamp(uv + vec2(dx/float(MAGE_viewwidth), dy/float(MAGE_viewheight)), 0.5f/MAGE_viewwidth, 1-0.5f/MAGE_viewheight));
}

bool canline(vec2 uv, float dx, float dy) {
    vec4 pix = at(uv, dx, dy);
    return pix.a == 1 && (pix.r != 0 || pix.g != 0 || pix.b != 0);
}

vec4 brighten(vec4 v) {
    float d = max(v.x, max(v.y, v.z));
    float scale = 1.0f / d;
    return vec4(v.x * scale, v.y * scale, v.z * scale, 1);
}

bool canline(vec2 uv, int rad) {
    for (int i = 0; i < 4 * rad; i ++) {
        float angle = PI * 2 * (i / (4.0f * rad));
        if (canline(uv, rad * cos(angle), rad * sin(angle))) return true;
    }
    return false;
}

void main() {
    vec4 color = texture(fb, vsh_texcoord);
    if (color.a < 1) {
        if (canline(vsh_texcoord, 1)) {
            fsh_color = vec4(0, 0, 0, 1);
        } else discard;
    } else fsh_color = color;
}