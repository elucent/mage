#version 330

in vec3 vsh_pos;

out vec4 fsh_color;

uniform float MAGE_viewdepth;

void main() {
    float bright = clamp(gl_FragCoord.z / (128 * gl_FragCoord.w), 0, 1);
    fsh_color = vec4(bright, bright, bright, 1);
}