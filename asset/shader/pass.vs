#version 330
layout(location = 0) in vec3 MAGE_position;
layout(location = 1) in vec2 MAGE_texcoord;

out vec2 vsh_texcoord;

uniform mat4 MAGE_projection, MAGE_camera, MAGE_transform;

void main() {
    gl_Position = MAGE_projection * MAGE_camera * MAGE_transform * vec4(MAGE_position, 1);

    vsh_texcoord = MAGE_texcoord;
}