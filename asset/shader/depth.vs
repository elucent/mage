#version 330
layout(location = 0) in vec3 MAGE_position;

out vec2 vsh_texcoord;

uniform mat4 MAGE_projection, MAGE_camera, MAGE_transform;
uniform int MAGE_viewwidth, MAGE_viewheight;

void main() {
    gl_Position = MAGE_projection * MAGE_camera * MAGE_transform * vec4(MAGE_position, 1);
}