#version 330

in vec2 vsh_texcoord;

out vec4 fsh_color;

uniform sampler2D fb;

void main() {
    fsh_color = texture(fb, vsh_texcoord);
}