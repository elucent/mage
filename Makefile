.PHONY: mage game tests test_vectors test_matrix

CC=clang++
CFLAGS=-std=c++14 -g3
INCLUDE=-Ilib/include -Iinclude
LIBS=-L. -lalut -lopenal -lrt -lm -ldl -lX11 -lpthread -lXrandr -lXinerama -lXxf86vm -lXcursor -lglfw -lGL -lSOIL
LIBSRC=$(lib/src/glad/*.c) $(lib/src/glad/*.cpp) 
MAGESRC=$(src/Lib/**/*.cpp) $(src/Predefined/**/*.cpp) $(src/Rendering/**/*.cpp)
SRCS=$(lib/src/glad/*.c) $(lib/src/glad/*.cpp) $(src/Lib/**/**/*.cpp) $(src/Predefined/**/*.cpp) $(src/Rendering/**/*.cpp)
OBJS := $(patsubst %.c, %.o, $(LIBSRC)) $(patsubst %.c, %.o, $(LIBSRC)) $(patsubst %.cpp, %.o, $(MAGESRC))

mage:
	clang++ -std=c++14 -g3 -Iinclude -Ilib/include \
	-L. -lopenal -lalut -lrt -lm -ldl -lX11 -lpthread -lXrandr -lXinerama -lXxf86vm -lXcursor -lglfw -lGL -lSOIL \
	lib/src/**/*.c src/Lib/**/*.cpp src/Predefined/**/*.cpp src/Rendering/**/*.cpp src/Mage.cpp -o mage
	./mage
	
test_vectors:
	clang++ -std=c++14 -g3 -Iinclude \
	-L. -lopenal -lalut -lrt -lm -ldl -lX11 -lpthread -lXrandr -lXinerama -lXxf86vm -lXcursor -lglfw3 -lGL -lSOIL \
	src/Math/*.cpp src/Game/*.cpp src/Utility/*.cpp src/Testing/*.cpp test/Vectors.cpp -o a.out
	./a.out
	rm a.out

test_matrix:
	clang++ -std=c++14 -g3 -Iinclude \
	-L. -lopenal -lalut -lrt -lm -ldl -lX11 -lpthread -lXrandr -lXinerama -lXxf86vm -lXcursor -lglfw3 -lGL -lSOIL \
	src/Math/*.cpp src/Game/*.cpp src/Utility/*.cpp src/Testing/*.cpp test/Matrix.cpp -o a.out
	./a.out
	rm a.out

tests: test_vectors test_matrix
