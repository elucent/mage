#include "Testing/Testing.h"

#include <iostream>

namespace mage {
    using namespace std;

    map<string, __TESTFN> __TEST_FUNCS;
    map<string, bool> __TEST_RESULTS;

    bool __TEST_ALL() {
        for (auto &p : __TEST_FUNCS) __TEST_RESULTS[p.first] = true;

        cout << "Prepared to execute " << __TEST_FUNCS.size() << " tests:" << endl;
        cout << endl;
        for (auto &p : __TEST_FUNCS) {
            cout << "Running test '" << p.first << "'..." << endl;
            p.second(p.first);
            if (__TEST_RESULTS[p.first]) {
                cout << "    pass!" << endl;
            }
            else {
                cout << "    fail!" << endl;
            }
        }
        cout << endl;

        cout << "Final test results:" << endl;
        int count = 0;
        for (auto &p : __TEST_RESULTS) {
            cout << "Test '" << p.first << "': " << (p.second ? "pass!" : "fail!") << endl;
            if (p.second) count ++;
        }
        cout << count << " / " << __TEST_RESULTS.size() << " passed!" << endl;
        cout << endl;
        
        return true;
    }
}