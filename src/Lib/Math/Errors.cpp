#include "Mage.h"
#include <iostream>

namespace mage {
    using namespace std;
        
    ArithmeticError::ArithmeticError(const string& message_in)
        : message("[ERROR] " + message_in) {
        //
    }

    const char* ArithmeticError::what() const noexcept {
        return message.c_str();
    }

    DataError::DataError(const string& message_in) 
        : message("[ERROR] " + message_in) {
        //
    }

    const char* DataError::what() const noexcept {
        return message.c_str();
    }
}