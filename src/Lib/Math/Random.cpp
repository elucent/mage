#include "Mage.h"

namespace mage {
    using namespace std;

    static mt19937_64 generator;

    bool RNGBool() {
        return generator() % 2;
    }

    int RNGInt() {
        return generator();
    }

    int RNGInt(int high) { 
        return generator() % high;
    }

    int RNGInt(int low, int high) {
        return low + generator() % (high - low);
    }

    float RNGFloat() {
        return (float)generator() / UINT_FAST64_MAX;
    }

    float RNGFloat(float high) {
        return RNGFloat() * high;
    }

    float RNGFloat(float low, float high) {
        return low + RNGFloat() * (high - low);
    }

    double RNGDouble() {
        return (double)generator() / UINT_FAST64_MAX;
    }

    double RNGDouble(double high) {
        return RNGDouble() * high;
    }

    double RNGDouble(double low, double high) {
        return low + RNGDouble() * (high - low);
    }
}