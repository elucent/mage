#include "Mage.h"
#include <cmath>

namespace mage {
    using namespace std;
    
    float Abs(float d) {
        return abs(d);
    }

    float Clamp(float d, float l, float h) {
        if (d < l) return l;
        if (d > h) return h;
        return d;
    }
    
    float Fract(float d) {
        return d - (int)d;
    }

    int Floor(float d) {
        return (int)floor(d);
    }

    int Ceil(float d) {
        return (int)ceil(d);
    }

    int Signum(float d) {
        return (int)(Abs(d) / d);
    }

    float Sqrt(float d) {
        return sqrt(d);
    }

    float Sin(float d) {
        return sin(d);
    }

    float Exp(float d) {
        return exp(d);
    }

    float Pow(float val, float power) {
        return pow(val, power);
    }

    float Cos(float d) {
        return cos(d);
    }

    float Tan(float d) {
        return tan(d);
    }

    float Csc(float d) {
        return 1. / Sin(d);
    }

    float Sec(float d) {
        return 1. / Cos(d);
    }

    float Cot(float d) {
        return 1. / Tan(d);
    }

    float Asin(float d) {
        return asin(d);
    }

    float Acos(float d) {
        return acos(d);
    }

    float Atan(float d) {
        return atan(d);
    }

    float Atan(float y, float x) {
        return atan2(y, x);
    }

    float Degrees(float d) {
        return 180. * (d / PI);
    }

    float Radians(float d) {
        return PI * (d / 180.);
    }

    float Fmod(float lhs, float rhs) {
        return lhs - rhs * floor(lhs / rhs);
    }

    bool Fequal(float lhs, float rhs) {
        return Abs(lhs - rhs) < 0.000001;
    }

    float Min(float lhs, float rhs) {
        return lhs < rhs ? lhs : rhs;
    }

    float Max(float lhs, float rhs) {
        return lhs < rhs ? rhs : lhs;
    }
}