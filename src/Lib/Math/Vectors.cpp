#include "Mage.h"

namespace mage {
    using namespace std;

    Vec2 Radial(float angle) {
        float ra = Radians(angle);
        return { Cos(ra), Sin(ra) };
    }

    Vec3 Radial(float pitch, float yaw) {
        float rp = Radians(pitch), ry = Radians(180+yaw);
        return { Cos(rp) * Sin(ry), Sin(rp), Cos(rp) * Cos(ry) };
    }
}