#include "Mage.h"

namespace mage {
    using namespace std;

    Mat4 Identity() {
        Mat4 m;
        for (int i = 0; i < 4; i ++) m(i, i) = 1;
        return m;
    }

    Mat4 Rotate(float angle, const Vec<3>& axis) {
        float ca = Cos(Radians(angle)), sa = Sin(Radians(angle));
        if (axis == Vec3{ 1, 0, 0 }) {
            return { 
                1, 0, 0, 0,
                0, ca, -sa, 0,
                0, sa, ca, 0,
                0, 0, 0, 1
            };
        }
        else if (axis == Vec3{ 0, 1, 0 }) {
            return {
                ca, 0, sa, 0,
                0, 1, 0, 0,
                -sa, 0, ca, 0,
                0, 0, 0, 1
            };
        }
        else if (axis == Vec3{ 0, 0, 1 }) {
            return {
                ca, -sa, 0, 0,
                sa, ca, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
        }
        else return {
            ca + axis[0] * axis[0] * (1 - ca), axis[0] * axis[1] * (1 - ca) - axis[2] * sa, axis[0] * axis[2] * (1 - ca) + axis[1] * sa, 0,
            axis[1] * axis[0] * (1 - ca) + axis[2] * sa, ca + axis[1] * axis[1] * (1 - ca), axis[1] * axis[2] * (1 - ca) - axis[0] * sa, 0,
            axis[2] * axis[0] * (1 - ca) - axis[1] * sa, axis[2] * axis[1] * (1 - ca) - axis[0] * sa, ca + axis[2] * axis[2] * (1 - ca), 0,
            0, 0, 0, 1
        };
    }

    Mat4 Scale(const Vec<3>& scale) {
        return {
            scale[0], 0, 0, 0,
            0, scale[1], 0, 0,
            0, 0, scale[2], 0,
            0, 0, 0, 1
        };
    }

    Mat4 Translate(const Vec<3>& translation) {
        return {
            1, 0, 0, translation[0],
            0, 1, 0, translation[1],
            0, 0, 1, translation[2],
            0, 0, 0, 1
        };
    }

    Mat4 Ortho(float left, float right, float top, float bottom, float near, float far) {
        return {
            2 / (right - left), 0, 0, (right + left) / (left - right),
            0, 2 / (top - bottom), 0, (top + bottom) / (bottom - top),
            0, 0, 2 / (near - far),  (far + near) / (near - far),
            0, 0, 0, 1
        };
    }

    Mat4 Perspective(float fov, float aspect, float near, float far) {
        float xmax, ymax;
        ymax = near * Tan(Radians(fov / 2));
        xmax = aspect * ymax;
        return Frustum(-xmax, xmax, ymax, -ymax, near, far);
    }

    Mat4 Frustum(float left, float right, float top, float bottom, float near, float far) {
        float t1, t2, t3, t4;
        t1 = 2 * near;
        t2 = right - left;
        t3 = top - bottom;
        t4 = far - near;
        return Mat4{
            t1 / t2, 0, 0, 0,
            0, t1 / t3, 0, 0,
            (right + left) / t2, (top + bottom) / t3, (-far - near) / t4, -1,
            0, 0, (-t1 * far) / t4, 0
        }.transpose();
    }
}