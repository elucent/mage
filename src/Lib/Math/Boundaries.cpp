#include "Mage.h"

namespace mage {
    using namespace std;

    // BoundingBox

    BoundingBox::BoundingBox(const Vec3& negative_extent, const Vec3& positive_extent) 
        : x(negative_extent[0]), y(negative_extent[1]), z(negative_extent[2]),
          w(positive_extent[0] - negative_extent[0]), h(positive_extent[1] - negative_extent[1]),
          l(positive_extent[2] - negative_extent[2]) {
        //
    }

    BoundingBox::BoundingBox(float x_in, float y_in, float z_in, float w_in, float h_in, float l_in) 
        : x(x_in), y(y_in), z(z_in), w(w_in), h(h_in), l(l_in) {
        //
    }

    float BoundingBox::getX() const {
        return x;
    }

    float BoundingBox::getY() const {
        return y;
    }

    float BoundingBox::getZ() const {
        return z;
    }

    float BoundingBox::getWidth() const {
        return w;
    }

    float BoundingBox::getHeight() const {
        return h;
    }

    float BoundingBox::getLength() const {
        return l;
    }

    float BoundingBox::getMinDimension() const {
        return Min(w, h, l);
    }

    Vec3 BoundingBox::getCenter() const {
        return { x + w / 2, y + h / 2, z + l / 2 };
    }

    float BoundingBox::volume() const {
        return x * y * z;
    }

    bool BoundingBox::intersects(const BoundingBox& box) const {
        return (x < box.x + box.w && x + w > box.x &&
                y < box.y + box.h && y + h > box.y &&
                z < box.z + box.l && z + l > box.z);
    }

    Vec3 BoundingBox::escape(const BoundingBox& box) const {
        bool dx = false, dy = false, dz = false;
        Vec3 dimRatios = Vec3{ box.w, box.h, box.l } / box.getMinDimension();
        Vec3 diff = ((getCenter() - box.getCenter()) / dimRatios).normalize();
        float max = Max(Abs(diff));
        if (Fequal(max, Abs(diff[0]))) {
            if (diff[0] < 0) return { box.x - (x + w), 0, 0 };
            else return { (box.x + box.w) - x, 0, 0 };
        }
        else if (Fequal(max, Abs(diff[1]))) {
            if (diff[1] < 0) return { 0, box.y - (y + h), 0 };
            else return { 0, (box.y + box.h) - y, 0 };
        }
        else {
            if (diff[2] < 0) return { 0, 0, box.z - (z + l) };
            else return { 0, 0, (box.z + box.l) - z };
        }
    }
    
    void BoundingBox::move(const Vec3& vec) {
        x += vec[0];
        y += vec[1];
        z += vec[2];
    }

    // BoundingSphere

    BoundingSphere::BoundingSphere(const Vec3& center_in, float radius_in) 
        : x(center_in[0]), y(center_in[1]), z(center_in[2]), radius(radius_in) {
        //
    }

    BoundingSphere::BoundingSphere(float x_in, float y_in, float z_in, float radius_in) 
        : x(x_in), y(y_in), z(z_in), radius(radius_in) {
        //
    }

    float BoundingSphere::getX() const {
        return x;
    }

    float BoundingSphere::getY() const {
        return y;
    }

    float BoundingSphere::getZ() const {
        return z;
    }

    Vec3 BoundingSphere::getCenter() const {
        return { x, y, z };   
    }

    float BoundingSphere::getRadius() const {
        return radius;
    }

    float BoundingSphere::volume() const {
        return PI * 4.0f / 3.0f * radius * radius * radius;
    }

    bool BoundingSphere::intersects(const BoundingSphere& sphere) const {
        return (sphere.getCenter() - getCenter()).distSq() <= (sphere.radius + radius) * (sphere.radius + radius);
    }

    Vec3 BoundingSphere::escape(const BoundingSphere& sphere) const {
        Vec3 dir = (sphere.getCenter() - getCenter());
        float dist = dir.dist();
        dir /= dist;

        return dir * (dist - sphere.getRadius() - getRadius());
    }
    
    void BoundingSphere::move(const Vec3& vec) {
        x += vec[0];
        y += vec[1];
        z += vec[2];
    }
}