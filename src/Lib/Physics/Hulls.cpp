#include "Mage.h"

namespace mage {
    using namespace std;


    struct GJKEdge {
        Vec3 a, b;

        bool operator==(const GJKEdge& other) const {
            return a == other.a && b == other.b;
        }
    };

    struct GJKSimplex {
        Vec3 v[4];
        int size = 0;

        bool add(const Hull& a, const Hull& b, const Vec3& direction) {
            Vec3 p = a.extent(direction) - b.extent(-direction);
            v[size ++] = p;
            return Dot(direction, p) >= 0;
        }

        void remove(int index) {
            for (int i = index; i < 3; i ++) v[i] = v[i+1];
            size --;
        }
    };

    class GJKFace {
        Vec3 v[3];
        Vec3 normal;
    public:
        GJKFace(Vec3 a, Vec3 b, Vec3 c) : normal(Cross(b-a, c-a).normalize()) {
            v[0] = a, v[1] = b, v[2] = c;
        }

        Vec3& operator[](int i) { return v[i]; }

        const Vec3& getNormal() const { return normal; }

        Vec3 project(const Vec3& point) {
            return normal * Dot(normal, v[0] - point);
        }

        pair<GJKFace, GJKFace> subdivide(const Vec3& point, vector<GJKFace>& faces) {
            pair<GJKFace, GJKFace> p({ v[0], v[1], point }, { v[0], point, v[2] });
            v[0] = point;
            return p;
        }
    };
}

namespace std {
    template <>
    struct hash<mage::GJKEdge> {
        size_t operator()(const mage::GJKEdge& other) const {
            size_t h = *(size_t*)(&mage::PI);
            for (int i = 0; i < 3; i ++) h ^= hash<float>{}(other.a[i]);
            for (int i = 0; i < 3; i ++) h ^= hash<float>{}(other.b[i]);
            return h;
        }
    };
}

namespace mage {
    using namespace std;

    // Hull

    Hull::~Hull() {
        //
    }

    // pair<int, int> GJKNearestEdge(const vector<Vec3>& polytope, bool clockwise) {
    //     for (int i = 0; i < polytope.size(); i ++) {
    //         int j = i == polytope.size()-1 ? 0 : i + 1;
    //         Vec3 diff = polytope[j] - polytope[i];

    //         Vec3 edge = clockwise 
    //             ?  
        
    //     }
    // }

    Vec3 Hull::intersects(const Hull& other) const {
        GJKSimplex simplex;
        bool found = false;
        for (int i = 0; i < 16 && !found; i ++) {
            // cout << "A: " << simplex.v[3] << endl
            //      << "B: " << simplex.v[2] << endl
            //      << "C: " << simplex.v[1] << endl 
            //      << "D: " << simplex.v[0] << endl;
            switch (simplex.size) {
                case 0: {
                    if (!simplex.add(*this, other, (other.center()-center()).normalize())) return 0;
                    break;
                }
                case 1: {
                    if (!simplex.add(*this, other, (center()-other.center()).normalize())) return 0;
                    break;
                }
                case 2: {
                    auto dc = simplex.v[0] - simplex.v[1];
                    auto c0 = -simplex.v[1];
                    auto dir = Cross(dc, c0);
                    simplex.add(*this, other, dir.normalize());
                    break;
                }
                case 3: {
                    auto dc = simplex.v[0] - simplex.v[1];
                    auto c0 = -simplex.v[1];
                    auto dir = Cross(Cross(dc, c0), dc);
                    simplex.add(*this, other, dir.normalize());
                    break;
                }
                case 4: {
                    auto a0 = -simplex.v[3];
                    auto abc = Cross(simplex.v[2]-simplex.v[3], simplex.v[1]-simplex.v[3]);
                    auto acd = Cross(simplex.v[1]-simplex.v[3], simplex.v[0]-simplex.v[3]);
                    auto adb = Cross(simplex.v[0]-simplex.v[3], simplex.v[2]-simplex.v[3]);
                    if (Dot(abc, a0) > 0) {
                        simplex.remove(0);
                        if (!simplex.add(*this, other, abc.normalize())) return 0;
                    }
                    else if (Dot(adb, a0) > 0) {
                        simplex.remove(1);
                        if (!simplex.add(*this, other, adb.normalize())) return 0;
                    }
                    else if (Dot(acd, a0) > 0) {
                        simplex.remove(2);
                        if (!simplex.add(*this, other, acd.normalize())) return 0;
                    }
                    else found = true;
                }
            }
        }

        vector<GJKFace> polytope;
        polytope.emplace_back(simplex.v[3], simplex.v[2], simplex.v[1]); // ABC
        polytope.emplace_back(simplex.v[3], simplex.v[1], simplex.v[0]); // ACD
        polytope.emplace_back(simplex.v[3], simplex.v[0], simplex.v[2]); // ADB
        polytope.emplace_back(simplex.v[2], simplex.v[0], simplex.v[1]); // BDC

        bool near = false;
        Vec3 escape = { 0, 0, -1 };
        for (int i = 0; i < 1; i ++) {
            int min = 0;
            escape = polytope[0].project(0);
            for (int j = 1; j < polytope.size(); j ++) {
                Vec3 d = polytope[j].project(0);
                if (d.distSq() < escape.distSq()) escape = d, min = j;
            }
            Vec3 nv = escape.normalize();
        }

        return escape;
    }

    // SphereHull

    SphereHull::SphereHull(const BoundingSphere& sphere_in) 
        : sphere(sphere_in) {
        //
    }

    Vec3 SphereHull::center() const {
        return sphere.getCenter();
    }

    Vec3 SphereHull::extent(const Vec3& direction) const {
        return sphere.getCenter() + direction * sphere.getRadius();
    }

    BoundingSphere& SphereHull::getBox() {
        return sphere;
    }

    const BoundingSphere& SphereHull::getBox() const {
        return sphere;
    }

    // BoxHull

    BoxHull::BoxHull(const BoundingBox& box_in)
        : box(box_in) {
        //
    }

    Vec3 BoxHull::center() const {
        return box.getCenter();
    }

    Vec3 BoxHull::extent(const Vec3& direction) const {
        vector<Vec3> points;
        points.push_back({ box.getX(), box.getY(), box.getZ() });
        points.push_back({ box.getX(), box.getY(), box.getZ()+box.getLength() });
        points.push_back({ box.getX(), box.getY()+box.getHeight(), box.getZ() });
        points.push_back({ box.getX(), box.getY()+box.getHeight(), box.getZ()+box.getLength() });
        points.push_back({ box.getX()+box.getWidth(), box.getY(), box.getZ() });
        points.push_back({ box.getX()+box.getWidth(), box.getY(), box.getZ()+box.getLength() });
        points.push_back({ box.getX()+box.getWidth()+box.getHeight(), box.getY(), box.getZ() });
        points.push_back({ box.getX()+box.getWidth()+box.getHeight(), box.getY(), box.getZ()+box.getLength() });
        
        float max = Dot(points[0], direction);
        int max_index = 0;
        for (int i = 1; i < points.size(); i ++) {
            float f = Dot(points[i], direction);
            if (f > max) max = f, max_index = i;
        }
        return points[max_index];
    }
    
    BoundingBox& BoxHull::getBox() {
        return box;
    }

    const BoundingBox& BoxHull::getBox() const {
        return box;
    }

    // PolygonHull

    PolygonHull::PolygonHull(const vector<Vec3>& points_in) 
        : points(points_in) {
        for (Vec3& v : points) c += v;
        c /= points.size();
    }

    Vec3 PolygonHull::center() const {
        return c;
    }

    Vec3 PolygonHull::extent(const Vec3& direction) const {
        float max = Dot(points[0], direction);
        int max_index = 0;
        for (int i = 1; i < points.size(); i ++) {
            float f = Dot(points[i], direction);
            if (f > max) max = f, max_index = i;
        }
        return points[max_index];
    }

    vector<Vec3>& PolygonHull::getPoints() {
        return points;
    }
    
    const vector<Vec3>& PolygonHull::getPoints() const {
        return points;
    }
}