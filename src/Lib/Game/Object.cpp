#include "Mage.h"

namespace mage {
    using namespace std;

    // Object

    Space* Object::getSpace() {
        return space;
    }

    int Object::getId() const {
        return id;
    }

    void Object::validate(Space* space_in, int id_in) {
        id = id_in;
        valid = true;
        space = space_in;
        world = space_in->getWorld();
    }
     
    Object::Object(const BoundingBox& box_in, bool behaves_in, bool animated_in, bool collides_in) 
        : Object(box_in, nullptr, behaves_in, animated_in, collides_in) {
        //
    }
     
    Object::Object(const BoundingBox& box_in, Texture* texture_in, bool behaves_in, bool animated_in, bool collides_in) 
        : world(nullptr), space(nullptr), texture(texture_in), box(box_in), valid(false), behaves(behaves_in), animated(animated_in), collides(collides_in),
          wasCollided(false), alive(true) {
        //
    }
    
    World* Object::getWorld() {
        return world;
    }

    Texture* Object::getBatchTexture() {
        return texture;
    }

    void Object::setBatchTexture(Texture* texture_in) {
        texture = texture_in;
        if (!animated) world->rebake();
    }
    
    bool Object::hasBehavior() const {
        return behaves;
    }
    
    bool Object::isCollidable() const {
        return collides;
    }
    
    bool Object::isAnimated() const {
        return animated;
    }
    
    void Object::update(GameInstance& game) {
        box.move(motion * game.getTime().getDelta());

        wasCollided = false;
        pushout = Vec3();
        if (isCollidable()) {
            vector<BoundingBox> collided = world->getColliders(this);
            for (BoundingBox& b : collided) {
                if (box.intersects(b)) {
                    wasCollided = true;
                    Vec3 v = box.escape(b);
                    if (v[1] != 0) continue;
                    pushout += v;
                    move(v);
                    if (Dot(v, motion) < 0) {
                        if (v[0] != 0) motion[0] = 0;
                        else motion[2] = 0;
                    }
                }
            }
            for (BoundingBox& b : collided) {
                if (box.intersects(b)) {
                    wasCollided = true;
                    Vec3 v = box.escape(b);
                    pushout += v;
                    if (v[1] == 0) continue;
                    move(v);
                    if (Dot(v, motion) < 0) {
                        if (v[1] != 0) motion[1] = 0;
                    }
                }
            }
        }
    }
    
    void Object::draw(GameInstance& game) {
        //
    }
    
    void Object::drawInto(GameInstance& game, Drawing& drawing) {
        //
    }

    void Object::onCollide(Object* other) {
        //
    }

    bool Object::canCollide(Object* other) {
        return true;
    }
    
    void Object::move(const Vec3& motion) {
        box.move(motion);
        if (!animated) world->rebake();
    }

    void Object::moveTo(const Vec3& position) {
        box.move(position - Vec3{ box.getX(), box.getY(), box.getZ() });
        if (!animated) world->rebake();
    }
    
    const BoundingBox& Object::getBox() const {
        return box;
    }

    Vec3 Object::getPosition() const {
        return box.getCenter();
    }

    Vec3& Object::getMotion() {
        return motion;
    }

    void Object::kill() {
        alive = false;
    }

    bool Object::isAlive() const {
        return alive;
    }

    // SpaceLocation

    SpaceLocation::SpaceLocation(int x_in, int y_in, int z_in) 
        : x(x_in), y(y_in), z(z_in) {
        //
    }

    int SpaceLocation::getX() const {
        return x;
    }

    int SpaceLocation::getY() const {
        return y;
    }

    int SpaceLocation::getZ() const {
        return z;
    }

    bool SpaceLocation::operator==(const SpaceLocation& other) const {
        return x == other.x && y == other.y && z == other.z;
    }
    
    // Space

    Space::Space(World* world_in, int x, int y, int z) 
        : world(world_in), loc(x, y, z) {
        //
    }

    BoundingBox Space::getBounds() const {
        return BoundingBox(loc.getX() * DIMENSION, loc.getY() * DIMENSION, loc.getZ() * DIMENSION, DIMENSION, DIMENSION, DIMENSION);
    }

    void Space::draw(GameInstance& game) {
        for (auto &p : objects) p.second->draw(game);
    }
    
    void Space::update(GameInstance& game) {
        for (auto &p : objects) p.second->update(game);
    }
    
    World* Space::getWorld() {
        return world;
    }
    
    // World

    World::World()
        : id(0), dirty(true) {
        //
    }

    Object* World::findObject(int id) {
        auto it = objects.find(id);
        if (it != objects.end()) return it->second;
        return nullptr;
    }
    
    void World::putObject(Object* object) {
        if (!object) return;
        Space* space = findSpace(object->getPosition());
        id ++;
        while (objects.find(id) != objects.end()) id ++;
        object->validate(space, id);
        //space->objects.emplace(id, object);
        objects.emplace(id, object);
        if (object->hasBehavior()) tickingObjects.insert(object);
        if (object->isAnimated()) animatedObjects.insert(object);
    }
    
    void World::removeObject(Object* object) {
       if (!object->isAnimated()) rebake();
        //Space* space = object->getSpace();
        //space->objects.erase(object->getId());
        //if (space->objects.size() == 0) spaces.erase(space->loc);
        objects.erase(object->getId());
        tickingObjects.erase(object);
        animatedObjects.erase(object);
        delete object;
    }

    void World::removeObjects(const function<bool(Object*)>& predicate) {
        vector<Object*> temp;
        for (auto &p : objects) if (predicate(p.second)) temp.push_back(p.second);
        for (Object* o : temp) removeObject(o);
    }

    Space* World::findSpace(Vec3 position) {
        int x = Floor(position[0] / Space::DIMENSION);
        int y = Floor(position[1] / Space::DIMENSION);
        int z = Floor(position[2] / Space::DIMENSION);
        SpaceLocation loc(x, y, z);
        auto it = spaces.find(loc);
        if (it == spaces.end()) return spaces.emplace(loc, new Space(this, x, y, z)).first->second;
        else return it->second;
    }

    vector<BoundingBox> World::getColliders(Object* object) {
        vector<BoundingBox> boxes;
        for (auto &p : objects) if (p.second->isAlive() && p.second != object && p.second->isCollidable() && object->canCollide(p.second) && p.second->canCollide(object)) {
            if (p.second->getBox().intersects(object->getBox())) {
                boxes.push_back(p.second->getBox());
                object->onCollide(p.second);
                p.second->onCollide(object);
            }
        }
        return boxes;
    }
    
    vector<BoundingBox> World::getColliders(const BoundingBox& box) {
        vector<BoundingBox> boxes;
        for (auto &p : objects) if (p.second->isAlive() && p.second->isCollidable()) {
            if (true) boxes.push_back(p.second->getBox());
        }
        return boxes;
    }
    
    void World::update(GameInstance& game) {
        vector<Object*> toRemove;
        for (auto object : tickingObjects) object->update(game);
        for (auto object : tickingObjects) if (!object->isAlive()) toRemove.push_back(object);
        for (auto object : toRemove) removeObject(object);
    }
    
    void World::render(GameInstance& game) {
        State& state = game.getRenderer().getActiveTask().getState();

        if (dirty) {
            dirty = false;
            for (auto &p : buffers) delete p.second;
            buffers.clear();
            for (auto &p : objects) {
                if (!p.second->isAnimated()) {
                    Texture* tex = p.second->getBatchTexture();
                    if (tex) {
                        auto it = buffers.find((intptr_t)p.second->getBatchTexture());
                        if (it == buffers.end()) {
                            p.second->drawInto(game, *buffers.emplace((intptr_t)tex, new Drawing(game)).first->second);
                        }
                        else p.second->drawInto(game, *it->second);
                    }
                }
            }
        }

        for (auto &p : buffers) {
            state.setTexture("tex", (Texture*)p.first, 0);
            p.second->draw(game);
        }

        unordered_map<intptr_t, Drawing*> drawings;
        for (auto object : animatedObjects) {
            Texture* tex = object->getBatchTexture();
            if (tex) {
                auto it = drawings.find((intptr_t)object->getBatchTexture());
                if (it == drawings.end()) {
                    object->drawInto(game, *drawings.emplace((intptr_t)tex, new Drawing(game)).first->second);
                }
                else object->drawInto(game, *it->second);
            }
            else object->draw(game);
        }
        for (auto &p : drawings) {
            state.setTexture("tex", (Texture*)p.first, 0);
            p.second->draw(game);
            delete p.second;
        }
    }

    void World::rebake() {
        dirty = true;
    }

    void World::receive(TickEvent& event) {
        update(event.getGame());
    }

    void World::receive(RenderGeometryEvent& event) {
        render(event.getGame());
    }
}