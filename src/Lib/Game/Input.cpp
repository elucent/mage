#include "Mage.h"

namespace mage { 
    using namespace std;

    unordered_set<int> Keyboard::tapped;
    unordered_set<int> Keyboard::pressed;

    void Keyboard::update(GLFWwindow* window) {
        for (int i : pressed) 
            if (glfwGetKey(window, i) != GLFW_PRESS) pressed.erase(i);
            else if (tapped.find(i) != tapped.end()) tapped.erase(i);
    }

    bool Keyboard::isKeyDown(int key) {
        return pressed.find(key) != pressed.end();
    }

    bool Keyboard::isKeyTapped(int key) {
        return tapped.find(key) != tapped.end();
    }

    double Mouse::x = 0;
    double Mouse::y = 0;

    void Mouse::update(GLFWwindow* window) {
        glfwGetCursorPos(window, &x, &y);
    }

    double Mouse::getX() {
        return x;
    }

    double Mouse::getY() {
        return y;
    }
}