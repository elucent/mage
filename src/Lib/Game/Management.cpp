#include "Mage.h"
#include "event/Event.h"

namespace mage {
    using namespace std;

    // SetupInfo

    SetupInfo::SetupInfo() 
        : name("Game Window"), width(1920), height(1080), fps(120), resizable(false) {
        //
    }

    SetupInfo& SetupInfo::setName(const string& name_in) {
        name = name_in;
        return *this;
    }

    SetupInfo& SetupInfo::setWidth(int width_in) {
        width = width_in;
        return *this;
    }
    
    SetupInfo& SetupInfo::setHeight(int height_in) {
        height = height_in;
        return *this;
    }
    
    SetupInfo& SetupInfo::setFPS(int fps_in) {
        fps = fps_in;
        return *this;
    }
    
    SetupInfo& SetupInfo::setResizable(bool resizable_in) {
        resizable = resizable_in;
        return *this;
    }
    
    const string& SetupInfo::getName() const {
        return name;
    }
    
    int SetupInfo::getWidth() const {
        return width;
    }  
    
    int SetupInfo::getHeight() const {
        return height;
    }
    
    int SetupInfo::getFPS() const {
        return fps;
    }
    
    bool SetupInfo::isResizable() const {
        return resizable;
    }

    // TimeManager

    TimeManager::TimeManager(int target_fps) 
        : prev(0), scale(1), time(0), fps(target_fps) {
        //
    }

    void TimeManager::update() {
        double frametime = 1. / fps;
        dt = glfwGetTime() - prev;
        //cout << "frame latency: " << dt << endl;
        if (dt < frametime) {
            sleep((int)(1000 * (frametime - dt)));
            dt = glfwGetTime() - prev;
        }
        prev = glfwGetTime();
        time += dt;
    }

    void TimeManager::setScale(double scale_in) {
        scale = scale_in;
    }

    double TimeManager::getUnscaledDelta() const {
        return dt;
    }

    double TimeManager::getDelta() const {
        return dt * scale;
    }

    double TimeManager::getTotal() const {
        return time;
    }

    double TimeManager::getScale() const {
        return scale;
    }

    // RenderManager

    void RenderManager::updateProperties(GameInstance& instance) {
        //
    }

    RenderManager::RenderManager() 
        : active(nullptr) {
        //
    }

    RenderManager::~RenderManager() {
        for (auto &p : canvases) delete p.second;
        for (auto &p : shaders) delete p.second;
        for (auto p : taskqueue) delete p;
    }

    RenderTask& RenderManager::addTask(RenderTask* task) {
        tasks.emplace(task->getIdentifier(), taskqueue.size());
        taskqueue.push_back(task);
        return *task;
    }

    RenderTask& RenderManager::getTask(const string& identifier) {
        auto it = tasks.find(identifier);
        if (it == tasks.end()) {
            throw StateError(BuildString(
                "Attempted to access unknown render task '", identifier, "'!"
            ));
        }
        return *taskqueue[it->second];
    }

    RenderTask& RenderManager::getActiveTask() {
        if (active == nullptr) {
            throw StateError(BuildString(
                "Attempted to access active task while no task was active!"
            ));
        }
        return *active;
    }

    const RenderTask& RenderManager::getActiveTask() const {
        return *active;
    }

    void RenderManager::removeTask(const string& identifier) {
        auto it = tasks.find(identifier);
        if (it == tasks.end()) return;
        taskqueue.erase(taskqueue.begin() + tasks[identifier]);
        tasks.erase(identifier);
    }

    Shader* RenderManager::addShader(const string& name, const string& vsh, const string& fsh) {
        Shader* shader = new Shader(*this, name, vsh, fsh);
        shaders.emplace(name, shader);
        return shader;
    }

    Shader* RenderManager::getShader(const string& name) {
        auto it = shaders.find(name);
        if (it == shaders.end()) {
            throw StateError(BuildString(
                "Attempted to access unknown shader '", name, "'!"
            ));
        }
        return it->second;
    }

    Canvas* RenderManager::addCanvas(const string& name, int width, int height) {
        Canvas* canvas = new Canvas(name, width, height);
        canvases.emplace(name, canvas);
        return canvas;
    }

    Canvas* RenderManager::getCanvas(const string& name) {
        auto it = canvases.find(name);
        if (it == canvases.end()) {
            throw StateError(BuildString(
                "Attempted to access unknown canvas '", name, "'!"
            ));
        }
        return it->second;
    }

    void MessageCallback( GLenum source,
                GLenum type,
                GLuint id,
                GLenum severity,
                GLsizei length,
                const GLchar* message,
                const void* userParam ){
        static int error_count = 0;
        if (type != GL_DEBUG_TYPE_ERROR) return;
        cout << "[ERROR] Internal GL error: " << endl << endl;
        fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
                type, severity, message );
    }

    void RenderManager::init(GameInstance& instance) {
        gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glClearColor(0, 0, 0, 1);

        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(MessageCallback, 0);
    }

    void RenderManager::render(GameInstance& instance) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for (auto task : taskqueue) {
            active = task;
            task->render(instance);
        }
        active = nullptr;
    }

    // TextureManager

    TextureManager::TextureManager() {
        //
    }

    TextureManager::~TextureManager() {
        for (auto &p : textures) delete p.second;
    }

    Texture* TextureManager::addTexture(const string& name) {
        return textures.emplace(name, new Texture(name)).first->second;
    }

    Texture* TextureManager::getTexture(const string& name) {
        auto it = textures.find(name);
        if (it == textures.end()) return addTexture(name);
        return it->second;
    }

    const Texture* TextureManager::getTexture(const string& name) const {
        auto it = textures.find(name);
        if (it == textures.end()) return nullptr;
        return it->second;
    }
    
    // GameInstance

    void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            Keyboard::tapped.insert(key);
            Keyboard::pressed.insert(key);
        }
        else if (action == GLFW_RELEASE) {
            Keyboard::tapped.erase(key);
            Keyboard::pressed.erase(key);
        }
    }
    
    GameInstance::GameInstance(const SetupInfo& setup) 
        : width(setup.getWidth()), height(setup.getHeight()), name(setup.getName()), time(setup.getFPS()) {
        glfwInit();

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_RESIZABLE, setup.isResizable() ? GLFW_TRUE : GLFW_FALSE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        handle = glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr);
        glfwMakeContextCurrent(handle);

        glfwSetKeyCallback(handle, KeyCallback);

        renderer.init(*this);
    }

    GameInstance::~GameInstance() {
        glfwDestroyWindow(handle);
    }

    int GameInstance::getWidth() const {
        return width;
    }
    
    int GameInstance::getHeight() const {
        return height;
    }

    float GameInstance::getAspect() const {
        return (float)width / height;
    }

    TimeManager& GameInstance::getTime() {
        return time;
    }

    RenderManager& GameInstance::getRenderer() {
        return renderer;
    }

    TextureManager& GameInstance::getTextures() {
        return textures;
    }
    
    void GameInstance::run() {
        while (!glfwWindowShouldClose(handle)) {
            Keyboard::update(handle);
            glfwPollEvents();
            Mouse::update(handle);

            event::dispatch(TickEvent(*this));

            renderer.render(*this);

            time.update();

            glfwSwapBuffers(handle);
        }
    }

    // TickEvent

    TickEvent::TickEvent(GameInstance& game_in) 
        : game(game_in), dt(game.getTime().getDelta()) {
        //
    }

    double TickEvent::getDelta() const {
        return dt;
    }

    GameInstance& TickEvent::getGame() {
        return game;
    }
}