#include "Mage.h"

namespace mage {
    using namespace std;

    Buffer::Buffer() {
        reader = bytes.data();
    }

    Buffer::Buffer(size_t length_in) 
        : bytes(length_in, 0) {
        reader = bytes.data();
    }

    size_t Buffer::getSize() const {
        return bytes.size() == 0 ? 0 : ((&bytes[0] + bytes.size()) - reader);
    }

    bool Buffer::isEmpty() const {
        return bytes.size() == 0 || reader == (&bytes[0] + bytes.size());
    }

    void Buffer::clear() {
        vector<unsigned char>().swap(bytes);
        reset();
    }

    void Buffer::reset() {
        reader = bytes.data();
    }
}