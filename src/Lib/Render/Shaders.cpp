#include "Mage.h"

namespace mage {
    using namespace std;

    // Shader

    Shader::Shader(RenderManager& game, const string& name_in, const string& vsh, const string& fsh) 
        : name(name_in) {
        ifstream vf(vsh), ff(fsh);

        const GLchar* str;
        char c;
        string vsrc, fsrc;
        int length, status;
        GLuint vobj = glCreateShader(GL_VERTEX_SHADER), fobj = glCreateShader(GL_FRAGMENT_SHADER);

        while (vf >> noskipws >> c) vsrc += c;
        length = vsrc.size();
        str = vsrc.c_str();
        glShaderSource(vobj, 1, &str, &length);
        
        while (ff >> noskipws >> c) fsrc += c;
        length = fsrc.size();
        str = fsrc.c_str();
        glShaderSource(fobj, 1, &str, &length);

        glCompileShader(vobj);
        glGetShaderiv(vobj, GL_COMPILE_STATUS, &status);
        if (!status) {
            throw ShaderError(BuildString(
                "Could not compile shader '",
                vsh,
                "'!"
            ), GL_VERTEX_SHADER, vobj);
        }

        glCompileShader(fobj);
        glGetShaderiv(fobj, GL_COMPILE_STATUS, &status);
        if (!status) {
            throw ShaderError(BuildString(
                "Could not compile shader '",
                fsh,
                "'!"
            ), GL_FRAGMENT_SHADER, fobj);
        }
        
        id = glCreateProgram();
        glAttachShader(id, vobj);
        glAttachShader(id, fobj);
        glLinkProgram(id);
        glGetProgramiv(id, GL_LINK_STATUS, &status);
        if (!status) {
            throw ProgramError(BuildString(
                "Could not link program '",
                id,
                "'!"
            ), id);
        }

        glDeleteShader(vobj);
        glDeleteShader(fobj);

        GLint count;

        GLint size;
        GLenum type;
        const GLsizei buf = 32;
        GLchar name[buf];
        GLsizei namelen;

        glGetProgramiv(id, GL_ACTIVE_ATTRIBUTES, &count);

        for (int i = 0; i < count; i ++) {
            glGetActiveAttrib(id, i, buf, &namelen, &size, &type, name);
            int loc = glGetAttribLocation(id, name);

            if (!IsPropertyType(type)) {
                throw ProgramError(BuildString(
                    "Unsupported attribute type '", type, "'!"
                ), id);
                break;
            }
            attributes.addProperty(name, GetPropertyType(type));
            attrib_locations.emplace(name, loc);
        }

        glGetProgramiv(id, GL_ACTIVE_UNIFORMS, &count);

        for (int i = 0; i < count; i ++) {
            glGetActiveUniform(id, i, buf, &namelen, &size, &type, name);
            int loc = glGetUniformLocation(id, name);

            if (!IsPropertyType(type)) {
                throw ProgramError(BuildString(
                    "Unsupported uniform type '", type, "'!"
                ), id);
                break;
            }
            uniforms.addProperty(name, GetPropertyType(type));
            uniform_locations.emplace(name, loc);
        }

        /*cout << "Loaded shader " << GetName() << " with: " << endl;
        for (auto &p : attributes) {
            cout << "    ATTRIBUTE " << p.second << " " << p.first << endl;
        }
        for (auto &p : uniforms) {
            cout << "    UNIFORM " << p.second << " " << p.first << endl;
        }*/
    }

    Shader::~Shader() {
        glDeleteProgram(id);
    }

    const PropertySet& Shader::getAttribs() const {
        return attributes;
    }

    const PropertySet& Shader::getUniforms() const {
        return uniforms;
    }
    
    int Shader::getAttribLocation(const string& attrib) const {
        auto it = attrib_locations.find(attrib);
        if (it == attrib_locations.end()) {
            return -1;
        }
        return it->second;
    }
    
    int Shader::getUniformLocation(const string& uniform) const {
        auto it = uniform_locations.find(uniform);
        if (it == uniform_locations.end()) {
            return -1;
        }
        return it->second;
    }

    GLuint Shader::getId() const {
        return id;
    }

    const string& Shader::getName() const {
        return name;
    }
}