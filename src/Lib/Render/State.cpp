#include "Mage.h"

namespace mage {
    using namespace std;

    unordered_map<int, PropertyType> GL_TYPES = {
        { GL_INT, PropertyType::INT },
        { GL_FLOAT, PropertyType::FLOAT },
        { GL_FLOAT_VEC2, PropertyType::VECTOR2 },
        { GL_FLOAT_VEC3, PropertyType::VECTOR3 },
        { GL_FLOAT_VEC4, PropertyType::VECTOR4 },
        { GL_FLOAT_MAT4, PropertyType::MATRIX4 },
        { GL_SAMPLER_2D, PropertyType::TEXTURE }
    };

    unordered_map<PropertyType, int> PROPERTY_SIZES = {
        { PropertyType::INT, sizeof(int) },
        { PropertyType::FLOAT, sizeof(float) },
        { PropertyType::VECTOR2, sizeof(Vec2) },
        { PropertyType::VECTOR3, sizeof(Vec3) },
        { PropertyType::VECTOR4, sizeof(Vec4) },
        { PropertyType::MATRIX4, sizeof(Mat4) },
        { PropertyType::TEXTURE, sizeof(int) }
    };

    bool IsPropertyType(int gltype) {
        return GL_TYPES.find(gltype) != GL_TYPES.end();
    }

    PropertyType GetPropertyType(int gltype) {
        return GL_TYPES[gltype];
    }

    int GetPropertySize(PropertyType type) {
        auto it = PROPERTY_SIZES.find(type);
        if (it == PROPERTY_SIZES.end()) {
            throw StateError(BuildString(
                "Attempted to get size of unsupported property '",
                (int)type, "'!"
            ));
        }
        return it->second;
    }

    ostream& operator<<(ostream& os, PropertyType type) {
        switch (type) {
            case PropertyType::INT:
                return os << "INT";
            case PropertyType::FLOAT:
                return os << "FLOAT";
            case PropertyType::VECTOR2:
                return os << "VECTOR2";
            case PropertyType::VECTOR3:
                return os << "VECTOR3";
            case PropertyType::VECTOR4:
                return os << "VECTOR4";
            case PropertyType::MATRIX4:
                return os << "MATRIX4";
            case PropertyType::TEXTURE:
                return os << "TEXTURE";
        }
        return os;
    }

    // PropertySet

    PropertySet::PropertySet() {
        //
    }

    PropertySet& PropertySet::addProperty(const string& name, PropertyType type) {
        auto it = properties.find(name);
        if (it != properties.end()) {
            throw StateError(BuildString(
                "Property '", name, 
                "' already exists in property set!"
            ));
        }
        properties.emplace(name, type);
        return *this;
    }

    bool PropertySet::hasMember(const string& name) const {
        return properties.find(name) != properties.end();
    }

    PropertyType PropertySet::getMemberType(const string& name) const {
        return properties.find(name)->second;
    }

    bool PropertySet::isSubsetOf(const PropertySet& other) const {
        if (properties.size() > other.properties.size()) return false;

        for (auto &prop : properties) {
            auto it = other.properties.find(prop.first);
            if (it == other.properties.end() || it->second != prop.second) return false;
        }

        return true;
    }

    unordered_map<string, PropertyType>::const_iterator PropertySet::begin() const {
        return properties.begin();
    }

    unordered_map<string, PropertyType>::const_iterator PropertySet::end() const {
        return properties.end();
    }

    // UniformLayout

    UniformLayout::UniformLayout(const PropertySet& properties_in) 
        : size(0), properties(&properties_in) {
        for (auto &p : properties_in.properties) {
            layout.emplace(p.first, size);
            size += GetPropertySize(p.second);
        }
    }

    PropertyType UniformLayout::getUniformType(const string& name) const {
        return properties->getMemberType(name);
    }
    
    const PropertySet& UniformLayout::getProperties() const {
        return *properties;
    }

    int UniformLayout::getUniformPosition(const string& name) const {
        auto it = layout.find(name);
        if (it == layout.end()) {
            throw StateError(BuildString(
                "Attempted to get position for unknown uniform '", name, "'!"
            )); 
        }
        else return it->second;
    }

    int UniformLayout::getSize() const {
        return size;
    }

    // State

    void State::checkUniformValidity(const string& uniform, PropertyType type) const {
        if (!layout->getProperties().hasMember(uniform)) {
            throw StateError(BuildString(
                "Attempted to set uniform '", uniform, "', but active task does not contain it!"
            ));
        }
        if (layout->getUniformType(uniform) != type) {
            throw StateError(BuildString(
                "Attempted to set uniform '", uniform, "' of type '", layout->getUniformType(uniform),
                "' as type '", type, "'!"
            ));
        }
    }

    void State::apply(const string& uniform) const {
        if (!active) return;
        int i = active->getUniformLocation(uniform);
        if (i == -1) return;

        const unsigned char* loc = &data[0] + layout->getUniformPosition(uniform);
        switch (layout->getUniformType(uniform)) {
            case PropertyType::INT:
                glUniform1iv(i, 1, (const GLint*)loc);
                break;
            case PropertyType::FLOAT:
                glUniform1fv(i, 1, (const GLfloat*)loc);
                break;
            case PropertyType::VECTOR2:
                glUniform2fv(i, 1, (const GLfloat*)loc);
                break;
            case PropertyType::VECTOR3:
                glUniform3fv(i, 1, (const GLfloat*)loc);
                break;
            case PropertyType::VECTOR4:
                glUniform4fv(i, 1, (const GLfloat*)loc);
                break;
            case PropertyType::MATRIX4:
                glUniformMatrix4fv(i, 1, GL_FALSE, (const GLfloat*)loc);
                break;
            case PropertyType::TEXTURE:
                glUniform1iv(i, 1, (const GLint*)loc);
                break;
            default:
                break;
        }
    }

    State::State(const UniformLayout* layout_in)
        : layout(layout_in), active(nullptr), data(layout->getSize(), 0), boundtextures(8, nullptr) {
        //
    }

    void State::apply() const {
        for (int i = 0; i < boundtextures.size(); i ++) {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, boundtextures[i] ? boundtextures[i]->getId() : 0);
        }
        glActiveTexture(GL_TEXTURE0);

        glUseProgram(active->getId());
        for (auto &prop : layout->getProperties()) {
            apply(prop.first);
        }
    }

    Shader* State::getShader() {
        return active;
    }

    const Shader* State::getShader() const {
        return active;
    }

    void State::setShader(Shader* shader) {
        active = shader;
        apply();
    }

    void State::setInteger(const string& name, int value) {
        checkUniformValidity(name, PropertyType::INT);
        RawRead<int>(&data[0] + layout->getUniformPosition(name)) = value;
        apply(name);
    }

    void State::setFloat(const string& name, float value) {
        checkUniformValidity(name, PropertyType::FLOAT);
        RawRead<float>(&data[0] + layout->getUniformPosition(name)) = value;
        apply(name);
    }

    void State::setVector(const string& name, const Vec2& vector) {
        checkUniformValidity(name, PropertyType::VECTOR2);
        RawRead<Vec2>(&data[0] + layout->getUniformPosition(name)) = vector;
        apply(name);
    }

    void State::setVector(const string& name, const Vec3& vector) {
        checkUniformValidity(name, PropertyType::VECTOR3);
        RawRead<Vec3>(&data[0] + layout->getUniformPosition(name)) = vector;
        apply(name);
    }

    void State::setVector(const string& name, const Vec4& vector) {
        checkUniformValidity(name, PropertyType::VECTOR4);
        RawRead<Vec4>(&data[0] + layout->getUniformPosition(name)) = vector;
        apply(name);
    }

    void State::setMatrix(const string& name, const Mat4& matrix) {
        checkUniformValidity(name, PropertyType::MATRIX4);
        RawRead<Mat4>(&data[0] + layout->getUniformPosition(name)) = matrix;
        apply(name);
    }

    void State::setTexture(const string& name, const Texture* texture, int texpos) {
        checkUniformValidity(name, PropertyType::TEXTURE);
        glActiveTexture(GL_TEXTURE0 + texpos);
        glBindTexture(GL_TEXTURE_2D, texture ? texture->getId() : 0);
        boundtextures[texpos] = texture;
        RawRead<int>(&data[0] + layout->getUniformPosition(name)) = texpos;
        apply(name);

        if (texture) {
            setInteger("MAGE_texwidth", texture->getWidth());
            setInteger("MAGE_texheight", texture->getHeight());
        }
    }
}