#include "Mage.h"
#include <iostream>

namespace mage {
    using namespace std;

    // ShaderError
        
    ShaderError::ShaderError(const string& message_in, GLuint shadertype, GLuint shader)
        : message("[ERROR] " + message_in + "\n\n") {
        if (shadertype == GL_VERTEX_SHADER) message += "Vertex ";
        else message += "Fragment ";
        message += "shader log: \n";
        char buffer[512];
        int length;
        glGetShaderInfoLog(shader, 512, &length, buffer);
        message += buffer;
    }

    const char* ShaderError::what() const noexcept {
        return message.c_str();
    }

    // ProgramError

    ProgramError::ProgramError(const string& message_in, GLuint program) 
        : message("[ERROR] " + message_in + "\n\n") {
        message += "Program log: \n";
        char buffer[512];
        int length;
        glGetProgramInfoLog(program, 512, &length, buffer);
        message += buffer;
    }

    const char* ProgramError::what() const noexcept {
        return message.c_str();
    }

    // StateError

    StateError::StateError(const string& message_in)
        : message("[ERROR] " + message_in) {
        //
    }

    const char* StateError::what() const noexcept {
        return message.c_str();
    }

    // DrawError

    DrawError::DrawError(const string& message_in)
        : message("[ERROR] " + message_in) {
        //
    }

    const char* DrawError::what() const noexcept {
        return message.c_str();
    }
}