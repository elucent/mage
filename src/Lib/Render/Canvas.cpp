#include "Mage.h"

namespace mage {
    using namespace std;

    Canvas::Canvas(const string& name_in, int width_in, int height_in) 
        : name(name_in), texture(width_in, height_in) {
        GLint prev_fbo, prev_rbo, prev_tex;
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prev_fbo);
        glGetIntegerv(GL_RENDERBUFFER_BINDING, &prev_rbo);
        glGetIntegerv(GL_TEXTURE_BINDING_2D, &prev_tex);

        glGenFramebuffers(1, &framebuffer);
        glGenRenderbuffers(1, &renderbuffer);

        glBindTexture(GL_TEXTURE_2D, texture.getId());
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width_in, height_in);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.getId(), 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

        glBindFramebuffer(GL_FRAMEBUFFER, prev_fbo);
        glBindRenderbuffer(GL_RENDERBUFFER, prev_rbo);
        glBindTexture(GL_TEXTURE_2D, prev_tex);
    }

    Canvas::~Canvas() {
        glDeleteFramebuffers(1, &framebuffer);
        glDeleteRenderbuffers(1, &renderbuffer);
    }

    int Canvas::getWidth() const {
        return texture.getWidth();
    }

    int Canvas::getHeight() const {
        return texture.getHeight();
    }

    const Texture* Canvas::getTexture() const {
        return &texture;
    }

    void Canvas::bind() const {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    }

    void Canvas::unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
    }

    void Canvas::resize(int width_in, int height_in) {
        GLint prev_fbo, prev_rbo, prev_tex;
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prev_fbo);
        glGetIntegerv(GL_RENDERBUFFER_BINDING, &prev_rbo);
        glGetIntegerv(GL_TEXTURE_BINDING_2D, &prev_tex);

        texture = Texture(width_in, height_in);
        glDeleteFramebuffers(1, &framebuffer);
        glDeleteFramebuffers(1, &renderbuffer);

        glGenFramebuffers(1, &framebuffer);
        glGenRenderbuffers(1, &renderbuffer);

        glBindTexture(GL_TEXTURE_2D, texture.getId());
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width_in, height_in);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.getId(), 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

        glBindFramebuffer(GL_FRAMEBUFFER, prev_fbo);
        glBindRenderbuffer(GL_RENDERBUFFER, prev_rbo);
        glBindTexture(GL_TEXTURE_2D, prev_tex);
    }  
}