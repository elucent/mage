#include "Mage.h"

namespace mage {
        using namespace std;

    void Texture::useDefaultFlags() {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void Texture::createFromData(const GLubyte* data) {
        GLint previous;
        glGetIntegerv(GL_TEXTURE_BINDING_2D, &previous);
        glGenTextures(1, &id);

        glBindTexture(GL_TEXTURE_2D, id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        useDefaultFlags();
        glBindTexture(GL_TEXTURE_2D, previous);
    }
    
    Texture::Texture(const string& path) {
        int channels;
        GLubyte* data = SOIL_load_image(path.c_str(), &width, &height, &channels, GL_FALSE);
        createFromData(data);
        SOIL_free_image_data(data);
    }
    
    Texture::Texture(int width_in, int height_in) 
        : Texture(width_in, height_in, { 0, 0, 0, 1 }) {}
    
    Texture::Texture(int width_in, int height_in, const Vec4& color) 
        : width(width_in), height(height_in) {
        GLubyte r = (int)(color[0] * 255), g = (int)(color[1] * 255), b = (int)(color[2] * 255), a = (int)(color[3] * 255);
        GLubyte* pixels = new GLubyte[width * height * 4];
        for (int i = 0; i < width * height; i += 4) {
            pixels[i] = r;
            pixels[i + 1] = g;
            pixels[i + 2] = b;
            pixels[i + 3] = a;
        }
        
        createFromData(pixels);

        delete[] pixels;
    }
    
    Texture::~Texture() {
        glDeleteTextures(1, &id);
    }
    
    Texture::Texture(const Texture& other) 
        : width(other.getWidth()), height(other.getHeight()) {

        glBindTexture(GL_TEXTURE_2D, other.id);
        GLubyte* data = new GLubyte[width * height * 4];
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        
        createFromData(data);

        delete[] data;
    }
    
    Texture& Texture::operator=(const Texture& other) {
        glDeleteTextures(1, &id);

        width = other.getWidth();
        height = other.getHeight();

        glBindTexture(GL_TEXTURE_2D, other.id);
        GLubyte* data = new GLubyte[width * height * 4];
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        
        createFromData(data);

        delete[] data;

        return *this;
    }
    
    GLuint Texture::getId() const {
        return id;
    }

    int Texture::getWidth() const {
        return width;
    }
    
    int Texture::getHeight() const {
        return height;
    }
}