#include "Mage.h"

namespace mage {
    using namespace std;

    RenderTask::RenderTask(GameInstance& game, const string& identifier_in, Canvas* canvas_in) 
        : attrib(constructDefaultAttrib()), uniform(constructDefaultUniforms()),
        identifier(identifier_in), canvas(canvas_in), layout(uniform), clear_color({ 0, 0, 0, 1 }) {
        states.push(State(&layout));
    }
    
    RenderTask::RenderTask(GameInstance& game, const string& identifier_in, const PropertySet& uniform_in, Canvas* canvas_in) 
        : attrib(constructDefaultAttrib()), uniform(uniform_in),
        identifier(identifier_in), canvas(canvas_in), layout(uniform), clear_color({ 0, 0, 0, 1 }) {
        states.push(State(&layout));
    }
    
    RenderTask::RenderTask(GameInstance& game, const string& identifier_in, const PropertySet& attrib_in, const PropertySet& uniform_in, Canvas* canvas_in) 
        : attrib(attrib_in), uniform(uniform_in), identifier(identifier_in), canvas(canvas_in), layout(uniform), clear_color({ 0, 0, 0, 1 }) {
        states.push(State(&layout));
    }
    
    RenderTask::~RenderTask() {
        //
    }
    
    const string& RenderTask::getIdentifier() const {
        return identifier;
    }

    void RenderTask::render(GameInstance& instance) {
        int width, height;

        if (canvas) {
            canvas->bind();
            glViewport(0, 0, canvas->getWidth(), canvas->getHeight());
            width = canvas->getWidth();
            height = canvas->getHeight();
        }
        else {
            Canvas::unbind();
            glViewport(0, 0, instance.getWidth(), instance.getHeight());
            width = instance.getWidth();
            height = instance.getHeight();
        }

        glClearColor(clear_color[0], clear_color[1], clear_color[2], clear_color[3]);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        getState().apply();
        getState().setInteger("MAGE_viewwidth", width);
        getState().setInteger("MAGE_viewheight", height);
        getState().setFloat("MAGE_viewdepth", 1);

        call(instance);

        Canvas::unbind();
    }

    State& RenderTask::getState() {
        if (states.empty()) {
            throw StateError(BuildString(
                "Attempted to get state from task '", identifier,
                "' but no states exist on the stack!"
            ));
        }
        
        return states.top();
    }

    const State& RenderTask::getState() const {
        if (states.empty()) {
            throw StateError(BuildString(
                "Attempted to get state from task '", identifier,
                "' but no states exist on the stack!"
            ));
        }
        
        return states.top();
    }

    void RenderTask::pushState() {
        states.push(states.top());
    }

    void RenderTask::popState() {
        if (states.size() == 1) {
            throw StateError(BuildString(
                "State underflow in task '", identifier, "'!"
            ));
        }

        states.pop();
        states.top().apply();
    }

    const PropertySet& RenderTask::getAttribs() const {
        return attrib;
    }

    const PropertySet& RenderTask::getUniforms() const {
        return uniform;
    }

    PropertySet RenderTask::constructDefaultAttrib() const {
        PropertySet s;
        s.addProperty("MAGE_position", PropertyType::VECTOR3);
        s.addProperty("MAGE_color", PropertyType::VECTOR4);
        s.addProperty("MAGE_normal", PropertyType::VECTOR3);
        s.addProperty("MAGE_texcoord", PropertyType::VECTOR2);
        s.addProperty("MAGE_sprite", PropertyType::VECTOR4);
        return s;
    }

    PropertySet RenderTask::constructDefaultUniforms() const {
        PropertySet s;
        s.addProperty("MAGE_transform", PropertyType::MATRIX4);
        s.addProperty("MAGE_camera", PropertyType::MATRIX4);
        s.addProperty("MAGE_projection", PropertyType::MATRIX4);
        s.addProperty("MAGE_viewwidth", PropertyType::INT);
        s.addProperty("MAGE_viewheight", PropertyType::INT);
        s.addProperty("MAGE_viewdepth", PropertyType::FLOAT);
        s.addProperty("MAGE_texwidth", PropertyType::INT);
        s.addProperty("MAGE_texheight", PropertyType::INT);
        return s;
    }

    void RenderTask::setClearColor(const Vec4& v) {
        clear_color = v;
    }

    const Vec4& RenderTask::getClearColor() const {
        return clear_color;
    }
};