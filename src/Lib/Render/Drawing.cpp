#include "Mage.h"

namespace mage {
    using namespace std;

    void Drawing::put(GLuint attrib, float datum) {
        data[attrib].push_back(datum);
    }

    Drawing::Drawing(const RenderTask& task) 
        : data(5, vector<float>()), buffers(5, 0), dirty(false), vertex_count(0) {
        const PropertySet& attrib = task.getAttribs();
        if (attrib.hasMember(POSITION_ATTRIB_NAME)) {
            position_type = attrib.getMemberType(POSITION_ATTRIB_NAME);
            glGenBuffers(1, &buffers[POSITION_INDEX]);
        }
        if (attrib.hasMember(COLOR_ATTRIB_NAME)) {
            color_type = attrib.getMemberType(COLOR_ATTRIB_NAME);
            glGenBuffers(1, &buffers[COLOR_INDEX]);
        }
        if (attrib.hasMember(NORMAL_ATTRIB_NAME)) {
            normal_type = attrib.getMemberType(NORMAL_ATTRIB_NAME);
            glGenBuffers(1, &buffers[NORMAL_INDEX]);
        }
        if (attrib.hasMember(TEXCOORD_ATTRIB_NAME)) {
            texcoord_type = attrib.getMemberType(TEXCOORD_ATTRIB_NAME);
            glGenBuffers(1, &buffers[TEXCOORD_INDEX]);
        }
        if (attrib.hasMember(SPRITE_ATTRIB_NAME)) {
            sprite_type = attrib.getMemberType(SPRITE_ATTRIB_NAME);
            glGenBuffers(1, &buffers[SPRITE_INDEX]);
        }

        glGenBuffers(1, &elements);
    }

    Drawing::Drawing(GameInstance& game) 
        : Drawing(game.getRenderer().getActiveTask()) {
        //
    }

    Drawing::~Drawing() {
        for (GLuint &i : buffers) if (i) glDeleteBuffers(1, &i);

        glDeleteBuffers(1, &elements);
    }

    void Drawing::clear() {
        for (vector<float> &v : data) v.clear();
        indices.clear();
        vertex_count = 0;
        dirty = true;
    }

    void Drawing::draw(const RenderTask& task) {
        if (dirty) {
            for (size_t i = 0; i < buffers.size(); i ++) {
                if (buffers[i]) {
                    glBindBuffer(GL_ARRAY_BUFFER, buffers[i]);
                    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data[i].size(), &data[i][0], GL_DYNAMIC_DRAW);
                }
            }

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_DYNAMIC_DRAW);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            dirty = false;
        }

        const Shader* shader = task.getState().getShader();
        const PropertySet& attrib = shader->getAttribs();

        vector<int> locations;

        if (attrib.hasMember(POSITION_ATTRIB_NAME)) {
            int location = shader->getAttribLocation(POSITION_ATTRIB_NAME);
            locations.push_back(location);
            glEnableVertexAttribArray(location);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[POSITION_INDEX]);
            int size = attrib.getMemberType(POSITION_ATTRIB_NAME) == PropertyType::VECTOR2 ? 2 : 3;
            glVertexAttribPointer(location, size, GL_FLOAT, GL_FALSE, 0, 0);
        }
        else {
            throw DrawError(BuildString(
                "Attempted to draw drawing using shader that has no position attribute!"
            ));
        }

        if (attrib.hasMember(COLOR_ATTRIB_NAME)) {
            int location = shader->getAttribLocation(COLOR_ATTRIB_NAME);
            locations.push_back(location);
            glEnableVertexAttribArray(location);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[COLOR_INDEX]);
            int size = attrib.getMemberType(COLOR_ATTRIB_NAME) == PropertyType::VECTOR3 ? 3 : 4;
            glVertexAttribPointer(location, size, GL_FLOAT, GL_FALSE, 0, 0);
        }

        if (attrib.hasMember(NORMAL_ATTRIB_NAME)) {
            int location = shader->getAttribLocation(NORMAL_ATTRIB_NAME);
            locations.push_back(location);
            glEnableVertexAttribArray(location);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[NORMAL_INDEX]);
            int size = 3;
            glVertexAttribPointer(location, size, GL_FLOAT, GL_FALSE, 0, 0);
        }

        if (attrib.hasMember(TEXCOORD_ATTRIB_NAME)) {
            int location = shader->getAttribLocation(TEXCOORD_ATTRIB_NAME);
            locations.push_back(location);
            glEnableVertexAttribArray(location);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[TEXCOORD_INDEX]);
            int size = 2;
            glVertexAttribPointer(location, size, GL_FLOAT, GL_FALSE, 0, 0);
        }

        if (attrib.hasMember(SPRITE_ATTRIB_NAME)) {
            int location = shader->getAttribLocation(SPRITE_ATTRIB_NAME);
            locations.push_back(location);
            glEnableVertexAttribArray(location);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[SPRITE_INDEX]);
            int size = 4;
            glVertexAttribPointer(location, size, GL_FLOAT, GL_FALSE, 0, 0);
        }

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements);
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        for (int i : locations) {
            glDisableVertexAttribArray(i);
        }
    }

    void Drawing::draw(GameInstance& game) {
        draw(game.getRenderer().getActiveTask());
    }

    GLuint Drawing::getVertexCount() const {
        return vertex_count;
    }

    Drawing& Drawing::addPosition(float x, float y, float z) {
        if (position_type == PropertyType::VECTOR3) {
            put(POSITION_INDEX, x);
            put(POSITION_INDEX, y);
            put(POSITION_INDEX, z);
        }
        else {
            throw DrawError(BuildString(
                "Type mismatch! Provided '", PropertyType::VECTOR3,
                "' to drawing with position type '", position_type, "'."
            ));
        }
        dirty = true;
        return *this;
    }

    Drawing& Drawing::addPosition(float x, float y) {
        if (position_type == PropertyType::VECTOR3) {
            put(POSITION_INDEX, x);
            put(POSITION_INDEX, y);
            put(POSITION_INDEX, 0);
        }
        else if (position_type == PropertyType::VECTOR2) {
            put(POSITION_INDEX, x);
            put(POSITION_INDEX, y);
        }
        else {
            throw DrawError(BuildString(
                "Type mismatch! Provided '", PropertyType::VECTOR2,
                "' to drawing with position type '", position_type, "'."
            ));
        }
        dirty = true;
        return *this;
    }

    Drawing& Drawing::addPosition(Vec3 xyz) {
        return addPosition(xyz[0], xyz[1], xyz[2]);
    }

    Drawing& Drawing::addPosition(Vec2 xy) {
        return addPosition(xy[0], xy[1]);
    }

    Drawing& Drawing::addColor(float r, float g, float b, float a) {
        if (color_type == PropertyType::VECTOR4) {
            put(COLOR_INDEX, r);
            put(COLOR_INDEX, g);
            put(COLOR_INDEX, b);
            put(COLOR_INDEX, a);
        }
        else {
            throw DrawError(BuildString(
                "Type mismatch! Provided '", PropertyType::VECTOR4,
                "' to drawing with color type '", color_type, "'."
            ));
        }
        dirty = true;
        return *this;
    }

    Drawing& Drawing::addColor(float r, float g, float b) {
        if (color_type == PropertyType::VECTOR4) {
            put(COLOR_INDEX, r);
            put(COLOR_INDEX, g);
            put(COLOR_INDEX, b);
            put(COLOR_INDEX, 1);
        }
        else if (color_type == PropertyType::VECTOR3) {
            put(COLOR_INDEX, r);
            put(COLOR_INDEX, g);
            put(COLOR_INDEX, b);
        }
        else {
            throw DrawError(BuildString(
                "Type mismatch! Provided '", PropertyType::VECTOR3,
                "' to drawing with color type '", color_type, "'."
            ));
        }
        dirty = true;
        return *this;
    }

    Drawing& Drawing::addColor(Vec4 rgba) {
        return addColor(rgba[0], rgba[1], rgba[2], rgba[3]);
    }

    Drawing& Drawing::addColor(Vec3 rgb) {
        return addColor(rgb[0], rgb[1], rgb[2]);
    }

    Drawing& Drawing::addShape(const Shape& shape) {
        shape.addToDrawing(*this);

        dirty = true;
        return *this;
    }

    Drawing& Drawing::addNormal(float x, float y, float z) {
        put(NORMAL_INDEX, x);
        put(NORMAL_INDEX, y);
        put(NORMAL_INDEX, z);

        dirty = true;
        return *this;
    }
    
    Drawing& Drawing::addNormal(Vec3 xyz) {
        return addNormal(xyz[0], xyz[1], xyz[2]);
    }

    Drawing& Drawing::addTexcoord(float u, float v) {
        put(TEXCOORD_INDEX, u);
        put(TEXCOORD_INDEX, v);

        dirty = true;
        return *this;
    }
    
    Drawing& Drawing::addTexcoord(Vec2 uv) {
        return addTexcoord(uv[0], uv[1]);
    }

    Drawing& Drawing::addSprite(float x, float y, float w, float h) {
        put(SPRITE_INDEX, x);
        put(SPRITE_INDEX, y);
        put(SPRITE_INDEX, w);
        put(SPRITE_INDEX, h);

        dirty = true;
        return *this;
    }

    Drawing& Drawing::addSprite(const Sprite& sprite) {
        return addSprite(sprite[0], sprite[1], sprite[2], sprite[3]);
    }

    Drawing& Drawing::traceVertex(GLuint i, GLuint base, bool newvert) {
        if (newvert) vertex_count ++;
        indices.push_back(i + base);

        dirty = true;
        return *this;
    }

    Drawing& Drawing::traceTriangle(GLuint a, GLuint b, GLuint c, GLuint base) {
        vertex_count += 3;
        indices.push_back(a + base);
        indices.push_back(b + base);
        indices.push_back(c + base);

        dirty = true;
        return *this;
    }

    Drawing& Drawing::traceQuad(GLuint a, GLuint b, GLuint c, GLuint d, GLuint base) {
        vertex_count += 4;
        indices.push_back(a + base);
        indices.push_back(b + base);
        indices.push_back(c + base);
        indices.push_back(c + base);
        indices.push_back(d + base);
        indices.push_back(a + base);

        dirty = true;
        return *this;
    }

    Drawing& Drawing::addDrawing(const Drawing& other) {
        if (position_type != other.position_type 
            || color_type != other.color_type
            || normal_type != other.normal_type
            || texcoord_type != other.normal_type
            || sprite_type != other.sprite_type) {
            throw DrawError(BuildString(
                "Attempted to add incompatible drawing to drawing!"
            ));
        }

        for (int i = 0; i < data.size(); i ++) {
            for (int j = 0; j < other.data[i].size(); j ++) {
                data[i].push_back(other.data[i][j]);
            }
        }

        GLuint base = indices.size();
        for (int i = 0; i < other.indices.size(); i ++) {
            indices.push_back(other.indices[i] + base);
        }

        dirty = true;
        return *this;
    }

    // ShapePropertied

    ShapePropertied::ShapePropertied() 
        : color({ 1, 1, 1, 1 }), texcoords({ 0, 0, 1, 1 }), sprite({ 0, 0, 1, 1 }), lit(true) {
        //
    }

    ShapePropertied& ShapePropertied::setColor(float r, float g, float b) {
        color = { r, g, b, 1 };

        return *this;
    }

    ShapePropertied& ShapePropertied::setColor(float r, float g, float b, float a) {
        color = { r, g, b, a };
        
        return *this;
    }

    ShapePropertied& ShapePropertied::setColor(const Vec3& color_in) {
        return setColor(color_in[0], color_in[1], color_in[2]);
    }

    ShapePropertied& ShapePropertied::setColor(const Vec4& color_in) {
        return setColor(color_in[0], color_in[1], color_in[2], color_in[3]);
    }

    ShapePropertied& ShapePropertied::setTexcoords(float u, float v, float w, float h) {
        texcoords = { u, v, w, h };
        
        return *this;
    }

    ShapePropertied& ShapePropertied::setTexcoords(const Vec4& texcoords_in) {
        texcoords = texcoords_in;
        
        return *this;
    }

    ShapePropertied& ShapePropertied::setSprite(float x, float y, float w, float h) {
        sprite = { x, y, w, h };
        
        return *this;
    }

    ShapePropertied& ShapePropertied::setSprite(const Vec4& sprite_in) {
        sprite = sprite_in;
        
        return *this;
    }

    ShapePropertied& ShapePropertied::setUnlit() {
        lit = false;
        
        return *this;
    }

    ShapePropertied& ShapePropertied::setLit() { 
        lit = true;
        
        return *this;
    }

    // ShapeSquare

    ShapeSquare::ShapeSquare(const Vec3& upperleft_in, const Vec3& lowerright_in) 
        : upperleft(upperleft_in), lowerright(lowerright_in) {
        normal = Cross(Vec3{ lowerright[0], upperleft[1], lowerright[2] } - upperleft,
                       Vec3{ upperleft[0], lowerright[1], upperleft[2] } - upperleft).normalize();
    }

    void ShapeSquare::addToDrawing(Drawing& drawing) const {
        drawing.addPosition(upperleft)
         .addPosition(lowerright[0], upperleft[1], lowerright[2])
         .addPosition(lowerright)
         .addPosition(upperleft[0], lowerright[1], upperleft[2]);

        drawing.addTexcoord(texcoords[0], texcoords[1])
         .addTexcoord(texcoords[0] + texcoords[2], texcoords[1])
         .addTexcoord(texcoords[0] + texcoords[2], texcoords[1] + texcoords[3])
         .addTexcoord(texcoords[0], texcoords[1] + texcoords[3]);

        for (int i = 0; i < 4; i ++) {
            drawing.addColor(color);
            drawing.addNormal(lit ? Vec3() : normal);
            drawing.addSprite(sprite);
        }

        drawing.traceQuad(0, 1, 2, 3, drawing.getVertexCount());
    }

    // ShapeCircle 

    ShapeCircle::ShapeCircle(const Vec3& upperleft_in, const Vec3& lowerright_in, int steps_in)
        : upperleft(upperleft_in), lowerright(lowerright_in), steps(steps_in) {
        normal = Cross(Vec3{ lowerright[0], upperleft[1], lowerright[2] } - upperleft,
                       Vec3{ upperleft[0], lowerright[1], upperleft[2] } - upperleft).normalize();
    }

    void ShapeCircle::addToDrawing(Drawing& drawing) const {
        Vec3 center = (upperleft + lowerright) / 2;
        Vec3 xaxis = { center[0] - upperleft[0], 0, 0 }, yaxis = { 0, center[1] - upperleft[1], 0 };

        float wh = texcoords[2] / 2, hh = texcoords[3] / 2;
        float cu = texcoords[0] + wh, cv = texcoords[1] + hh;

        for (float i = 0; i < 2 * PI; i += 2 * PI / steps) {
            GLuint base = drawing.getVertexCount();
            float s = Sin(i), c = Cos(i);

            drawing.addPosition(center);
            drawing.addColor(color);
            drawing.addNormal({ 0, 0, -1 });
            drawing.addTexcoord(cu + wh * c, cv - hh * s);
            drawing.addSprite(sprite);

            drawing.addPosition(center + xaxis * c + yaxis * s);
            drawing.addColor(color);
            drawing.addNormal({ 0, 0, -1 });
            drawing.addTexcoord(cu + wh * c, cv - hh * s);
            drawing.addSprite(sprite);

            s = Sin(i + 2 * PI / steps), c = Cos(i + 2 * PI / steps);
            drawing.addPosition(center + xaxis * c + yaxis * s);
            drawing.addColor(color);
            drawing.addNormal({ 0, 0, -1 });
            drawing.addTexcoord(cu + wh * c, cv - hh * s);
            drawing.addSprite(sprite);

            drawing.traceTriangle(0, 1, 2, base);
        }
    }

    // ShapeTriangle

    ShapeTriangle::ShapeTriangle(const Vec3& a_in, const Vec3& b_in, const Vec3& c_in) 
        : a(a_in), b(b_in), c(c_in) {
        normal = Cross(b - a, c - a).normalize();
    }

    void ShapeTriangle::addToDrawing(Drawing& drawing) const {
        drawing.addPosition(a).addPosition(b).addPosition(c);

        drawing.addTexcoord(texcoords[0], texcoords[1])
               .addTexcoord(texcoords[0] + texcoords[2], texcoords[1])
               .addTexcoord(texcoords[0] + texcoords[2] / 2, texcoords[1] + texcoords[3]);

        for (int i = 0; i < 3; i ++) {
            drawing.addColor(color);
            drawing.addNormal(lit ? Vec3() : normal);
            drawing.addSprite(sprite);
        }

        drawing.traceTriangle(0, 1, 2, drawing.getVertexCount());
    }

    // ShapeQuad

    ShapeQuad::ShapeQuad(const Vec3& a_in, const Vec3& b_in, const Vec3& c_in, const Vec3& d_in) 
        : a(a_in), b(b_in), c(c_in), d(d_in) {
        normal = Cross(b - a, c - a).normalize();
    }

    void ShapeQuad::addToDrawing(Drawing& drawing) const {
        drawing.addPosition(a).addPosition(b).addPosition(c).addPosition(d);

        drawing.addTexcoord(texcoords[0], texcoords[1])
               .addTexcoord(texcoords[0] + texcoords[2], texcoords[1])
               .addTexcoord(texcoords[0] + texcoords[2], texcoords[1] + texcoords[3])
               .addTexcoord(texcoords[0], texcoords[1] + texcoords[3]);

        for (int i = 0; i < 4; i ++) {
            drawing.addColor(color);
            drawing.addNormal(normal);
            drawing.addSprite(sprite);
        }

        drawing.traceQuad(0, 1, 2, 3, drawing.getVertexCount());
    }

    // ShapeCube

    ShapeCube::ShapeCube(const Vec3& upperleft_in, const Vec3& lowerright_in, bool flat_in) 
        : upperleft(upperleft_in), lowerright(lowerright_in) {
        for (int i = 0; i < 6; i ++) sprites[i] = { 0, 0, 1, 1 }, facecoords[i] = { 0, 0, 1, 1 };
    }

    ShapeCube::ShapeCube(const BoundingBox& box_in, bool flat_in) 
        : ShapeCube({ box_in.getX(), box_in.getY() + box_in.getHeight(), box_in.getZ() },
                    { box_in.getX() + box_in.getWidth(), box_in.getY(), box_in.getZ() + box_in.getLength() },
                    flat_in) {
        //
    }

    ShapeCube& ShapeCube::setLeft(const Sprite& spr) {
        sprites[0] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::setRight(const Sprite& spr) {
        sprites[1] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::setTop(const Sprite& spr) {
        sprites[2] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::setBottom(const Sprite& spr) {
        sprites[3] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::setBack(const Sprite& spr) {
        sprites[4] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::setFront(const Sprite& spr) {
        sprites[5] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::setAll(const Sprite& spr) {
        for (int i = 0; i < 6; i ++) sprites[i] = spr;
        return *this;
    }

    ShapeCube& ShapeCube::autoTexcoords() {
        BoundingBox b({ upperleft[0], lowerright[1], upperleft[2] }, { lowerright[0], upperleft[1], lowerright[2] });
        float f = b.getMinDimension();
        facecoords[0] = (facecoords[1] = { 0, 0, b.getLength() / f, b.getHeight() / f });
        facecoords[2] = (facecoords[3] = { 0, 0, b.getWidth() / f, b.getLength() / f });
        facecoords[4] = (facecoords[5] = { 0, 0, b.getWidth() / f, b.getHeight() / f });
        return *this;
    }

    void ShapeCube::addToDrawing(Drawing& drawing) const {
        // Negative X
        drawing.addPosition(upperleft[0], upperleft[1], lowerright[2])
               .addPosition(upperleft[0], upperleft[1], upperleft[2])
               .addPosition(upperleft[0], lowerright[1], upperleft[2])
               .addPosition(upperleft[0], lowerright[1], lowerright[2]);

        // Positive X
        drawing.addPosition(lowerright[0], upperleft[1], upperleft[2])
               .addPosition(lowerright[0], upperleft[1], lowerright[2])
               .addPosition(lowerright[0], lowerright[1], lowerright[2])
               .addPosition(lowerright[0], lowerright[1], upperleft[2]);

        // Negative Y
        drawing.addPosition(lowerright[0], upperleft[1], upperleft[2])
               .addPosition(upperleft[0], upperleft[1], upperleft[2])
               .addPosition(upperleft[0], upperleft[1], lowerright[2])
               .addPosition(lowerright[0], upperleft[1], lowerright[2]);

        // Positive Y
        drawing.addPosition(upperleft[0], lowerright[1], upperleft[2])
               .addPosition(lowerright[0], lowerright[1], upperleft[2])
               .addPosition(lowerright[0], lowerright[1], lowerright[2])
               .addPosition(upperleft[0], lowerright[1], lowerright[2]);

        // Negative Z
        drawing.addPosition(upperleft[0], upperleft[1], upperleft[2])
               .addPosition(lowerright[0], upperleft[1], upperleft[2])
               .addPosition(lowerright[0], lowerright[1], upperleft[2])
               .addPosition(upperleft[0], lowerright[1], upperleft[2]);

        // Positive Z
        drawing.addPosition(lowerright[0], upperleft[1], lowerright[2])
               .addPosition(upperleft[0], upperleft[1], lowerright[2])
               .addPosition(upperleft[0], lowerright[1], lowerright[2])
               .addPosition(lowerright[0], lowerright[1], lowerright[2]);

        if (lit) {
            for (int i = 0; i < 4; i ++) drawing.addNormal({ -1, 0, 0 });
            for (int i = 0; i < 4; i ++) drawing.addNormal({ 1, 0, 0 });
            for (int i = 0; i < 4; i ++) drawing.addNormal({ 0, -1, 0 });
            for (int i = 0; i < 4; i ++) drawing.addNormal({ 0, 1, 0 });
            for (int i = 0; i < 4; i ++) drawing.addNormal({ 0, 0, -1 });
            for (int i = 0; i < 4; i ++) drawing.addNormal({ 0, 0, 1 });
        }
        else for (int i = 0; i < 24; i ++) drawing.addNormal(Vec3());

        for (int i = 0; i < 6; i ++) {
            const Vec4& v = facecoords[i];
            drawing.addTexcoord(v[0], v[1])
                   .addTexcoord(v[0] + v[2], v[1])
                   .addTexcoord(v[0] + v[2], v[1] + v[3])
                   .addTexcoord(v[0], v[1] + v[3]);
            for (int j = 0; j < 4; j ++) drawing.addSprite(sprites[i]);
        }

        for (int i = 0; i < 24; i ++) {
            drawing.addColor(color);
        }

        GLuint base = drawing.getVertexCount();

        for (int i = 0; i < 24; i += 4) {
            drawing.traceQuad(i, i + 1, i + 2, i + 3, base);
        }
    }
}