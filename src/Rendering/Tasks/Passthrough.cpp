#include "Mage.h"

namespace mage {
    using namespace std;

    void PassthroughTask::call(GameInstance& game) {
        Drawing d = Drawing(*this);
        d.addShape(ShapeSquare({ 0, 0, 0 }, { 480, 320, 0 }));
        d.draw(*this);
    }

    PassthroughTask::PassthroughTask(GameInstance& game, const string& identifier_in, Canvas* canvas, Shader* shader, Canvas* target) 
        : RenderTask(game, identifier_in, constructDefaultUniforms().addProperty("fb", PropertyType::TEXTURE), target) {
        getState().setShader(shader);
        getState().setMatrix("MAGE_projection", Ortho(0, 480, 320, 0, 0, 40));
        getState().setMatrix("MAGE_camera", Identity());
        getState().setMatrix("MAGE_transform", Identity());

        getState().setTexture("fb", canvas->getTexture(), 0);

        setClearColor({ 0, 0, 0, 0 });
    }

    void CombineTask::call(GameInstance& game) {
        getState().setShader(sa);
        getState().setTexture("fb", a->getTexture(), 0);

        Drawing d = Drawing(*this);
        d.addShape(ShapeSquare({ 0, 0, 0 }, { 480, 320, 0 }).setTexcoords(0, 1, 1, -1));
        d.draw(*this);

        getState().setShader(sb);
        getState().setTexture("fb", b->getTexture(), 0);
        
        d.clear();
        d.addShape(ShapeSquare({ 0, 0, 2 }, { 480, 320, 2 }));
        d.draw(*this);
    }

    CombineTask::CombineTask(GameInstance& game, const string& identifier_in, Canvas* canvas_a, Shader* shader_a, Canvas* canvas_b, Shader* shader_b, Canvas* target) 
        : RenderTask(game, identifier_in, constructDefaultUniforms().addProperty("fb", PropertyType::TEXTURE), target),
          a(canvas_a), b(canvas_b), sa(shader_a), sb(shader_b) {
        getState().setShader(sa);
        getState().setMatrix("MAGE_projection", Ortho(0, 480, 320, 0, -40, 40));
        getState().setMatrix("MAGE_camera", Identity());
        getState().setMatrix("MAGE_transform", Identity());

        setClearColor({ 0, 0, 0, 0 });
    }
}