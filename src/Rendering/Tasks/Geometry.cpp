#include "Mage.h"
#include "event/Event.h"

namespace mage {
    using namespace std;

    // Camera

    Camera::Camera(const Vec3 src, const Vec3 dest) 
        : pos(src), fov(70) {
        lookAt(dest);
    }
    
    Camera::Camera(const Vec3 pos_in, float pitch, float yaw, float roll) 
        : pos(pos_in), angles({ pitch, yaw, roll }), fov(70) {
    }

    Vec3& Camera::getPos() {
        return pos;
    }
    
    const Vec3& Camera::getPos() const {
        return pos;
    }
    
    void Camera::setPos(const Vec3& src_in) {
        pos = src_in;
    }

    Vec3& Camera::getAngles() {
        return angles;
    }
    
    const Vec3& Camera::getAngles() const {
        return angles;
    }
    
    void Camera::setAngles(const Vec3& in) {
        angles = in;
    }
    
    void Camera::lookAt(const Vec3& src_in) {
        //
    }
    
    float Camera::getFOV() const {
        return fov;
    }
    
    void Camera::setFOV(float fov_in) {
        fov = fov_in;
    }
    
    Mat4 Camera::getMatrix() const {
        return Rotate(-angles[0], { 1, 0, 0 }) * Rotate(-angles[1], { 0, 1, 0 }) * Rotate(-angles[2], { 0, 0, 1 }) * Translate(-pos);
    }
    
    // RenderGeometryEvent

    RenderGeometryEvent::RenderGeometryEvent(GameInstance& game_in, GeometryTask& task_in)
        : game(game_in), task(task_in) {
        //
    }

    GameInstance& RenderGeometryEvent::getGame() {
        return game;
    }

    GeometryTask& RenderGeometryEvent::getTask() {
        return task;
    }

    State& RenderGeometryEvent::getState() {
        return task.getState();
    }

    // GeometryTask

    void GeometryTask::call(GameInstance& game) {
        getState().setFloat("MAGE_viewdepth", 1000);
        getState().setMatrix("MAGE_projection", Perspective(getCamera().getFOV(), game.getAspect(), 0.1f, 1000));
        getState().setMatrix("MAGE_camera", getCamera().getMatrix());
        getState().setMatrix("MAGE_transform", Identity());

        event::dispatch(RenderGeometryEvent(game, *this));
    }

    GeometryTask::GeometryTask(GameInstance& game, const string& identifier, Shader* shader_in, Canvas* canvas) 
        : RenderTask(game, identifier, constructDefaultUniforms().addProperty("tex", PropertyType::TEXTURE), canvas), shader(shader_in) {
        cameras.push_back(Camera({ 0, 0, 0 }, 0, 0, 0));
        getState().setShader(shader);
        
        setClearColor({ 0, 0, 0, 0 });
    }
    
    Camera& GeometryTask::getCamera() {
        return cameras.back();
    }
    
    const Camera& GeometryTask::getCamera() const {
        return cameras.back();
    }
}