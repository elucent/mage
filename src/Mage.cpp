#include "Mage.h"
#include <iostream>
#include <string>

using namespace std;
using namespace mage;

class Collider : public event::Listener<RenderGeometryEvent> {
    vector<Hull*> colliders;
    vector<ShapePropertied*> shapes;

public:
    ~Collider() {
        for (Hull* p : colliders) delete p;
        for (ShapePropertied* p : shapes) delete p;
    }
    
    void addSphere(const BoundingSphere& sphere) {
        colliders.push_back(new SphereHull(sphere));
        Vec3 ul = sphere.getCenter() - Vec3{ sphere.getRadius(), sphere.getRadius(), 0 };
        Vec3 lr = sphere.getCenter() + Vec3{ sphere.getRadius(), sphere.getRadius(), 0 };
        shapes.push_back(new ShapeCircle(ul, lr, 64));
    }

    void addTri(const Vec3& a, const Vec3& b, const Vec3& c) {
        Vec3 h = { 0, 0, 1000 };
        colliders.push_back(new PolygonHull({ a - h, b - h, c - h, a + h, b + h, c + h }));
        shapes.push_back(new ShapeTriangle(a, b, c));
    }

    void receive(RenderGeometryEvent& event) override {
        event.getTask().getState().setTexture("tex", event.getGame().getTextures().getTexture("asset/tex/white.png"), 0);
        event.getTask().getState().setMatrix("MAGE_projection", Ortho(0, 960, 0, 640, -100, 100));

        static const Vec4 RED = { 1, 0.3, 0.3, 1 };
        static const Vec4 GREEN = { 0.3, 1, 0.3, 1 };

        BoundingSphere sphere = BoundingSphere({ (float)Mouse::getX(), (float)Mouse::getY(), 0 }, 40.0);
        SphereHull cursor(sphere);

        Drawing d(event.getTask());
        Vec3 escape;
        for (int i = 0; i < colliders.size(); i ++) {
            Vec3 temp;
            if ((temp = colliders[i]->intersects(cursor)) != Vec3(0)) shapes[i]->setColor(RED), escape = temp;
            else shapes[i]->setColor(GREEN);
            shapes[i]->setUnlit();

            d.addShape(*shapes[i]);
        }
        cout << escape << endl;
        d.addShape(ShapeCircle(sphere.getCenter() + escape - Vec3{ 40, 40, 0 }, sphere.getCenter() + escape + Vec3{ 40, 40, 0 }, 64).setColor(GREEN));
        d.draw(event.getTask());
    }
};

int main(int argc, char** argv) {
    GameInstance game(SetupInfo().setName("Collision Test.").setWidth(960).setHeight(640));
    game.getTime().setScale(1);
    
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    Shader* shDefault = game.getRenderer().addShader("default", "asset/shader/default.vs", "asset/shader/default.fs");
    
    // World world;
    // for (int i = -20; i < 21; i ++) {
    //     for (int j = -20; j < 21; j ++) {
    //         world.putObject(new ObjectBox({ -2.5f + 5 * i, -2.5f + RNGFloat(-2, 2), -2.5f + 5 * j, 5, 5, 5 }, game.getTextures().getTexture("asset/tex/debug.png")));
    //     }
    // }
    // world.putObject(new ObjectPlayer({ -0.375, 20, -0.375, 0.75, 1, 0.75 }));

    Collider c;
    c.addSphere({ { 720, 160, 0 }, 120 });
    c.addTri({ 240, 80, 0 }, { 120, 400, 0 }, { 480, 200, 0 });

    game.getRenderer().addTask(new GeometryTask(game, "geometry", shDefault, nullptr));

    game.run();
    return 0;
}