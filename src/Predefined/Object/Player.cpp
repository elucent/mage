#include "Mage.h"

namespace mage {
    using namespace std;

    ObjectPlayer::ObjectPlayer(const BoundingBox& box) 
        : Object(box), pitch(0), yaw(0) {
        //
    }

    void ObjectPlayer::update(GameInstance& game) {
        float dt = (float)game.getTime().getDelta();
        
        if (Keyboard::isKeyDown(GLFW_KEY_UP)) pitch += 90.0f * dt;
        if (Keyboard::isKeyDown(GLFW_KEY_DOWN)) pitch -= 90.0f * dt;
        if (Keyboard::isKeyDown(GLFW_KEY_LEFT)) yaw += 90.0f * dt;
        if (Keyboard::isKeyDown(GLFW_KEY_RIGHT)) yaw -= 90.0f * dt;

        if (Keyboard::isKeyDown(GLFW_KEY_W)) motion += (Radial(0, yaw) * 40.0f * dt);
        if (Keyboard::isKeyDown(GLFW_KEY_S)) motion += (Radial(0, yaw) * -40.0f * dt);
        if (Keyboard::isKeyDown(GLFW_KEY_A)) motion += (Radial(0, yaw + 90) * 40.0f * dt);
        if (Keyboard::isKeyDown(GLFW_KEY_D)) motion += (Radial(0, yaw - 90) * 40.0f * dt);
        Vec2 v = { motion[0], motion[2] };
        if (v.distSq() > 100.0f) v = v.normalize() * 5;
        motion[0] = v[0] * 0.9f;
        motion[2] = v[1] * 0.9f;
        motion[1] -= 20.0f * dt;

        if (Keyboard::isKeyTapped(GLFW_KEY_SPACE)) {
            motion[1] = 6.0f;
        }

        Object::update(game);

        GeometryTask& task = (GeometryTask&)game.getRenderer().getTask("geometry");
        task.getCamera().setPos(box.getCenter() + Vec3{ 0, 0.25f, 0 });
        task.getCamera().getAngles()[0] = pitch;
        task.getCamera().getAngles()[1] = yaw;
    }

    void ObjectPlayer::draw(GameInstance& game) {
        //
    }
}