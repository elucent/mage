#include "Mage.h"

namespace mage {
    using namespace std;

    // ObjectBox
    
    ObjectBox::ObjectBox(const BoundingBox& box, Texture* texture) 
        : Object(box, texture, false, false), cube(box) {
        cube.autoTexcoords();
    }

    ShapeCube& ObjectBox::getCube() {
        return cube;
    }

    void ObjectBox::drawInto(GameInstance& game, Drawing& d) {
        d.addShape(cube);
    }
}