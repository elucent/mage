#ifndef MAGE_RENDER_TASK_H
#define MAGE_RENDER_TASK_H

#include "Definitions.h"
#include "Lib/Render/Canvas.h"
#include "Lib/Render/Shaders.h"
#include <string>
#include <stack>

namespace mage {
    using namespace std;

    class RenderTask {
        PropertySet attrib, uniform;
        string identifier;
        Canvas* canvas;
        UniformLayout layout;
        Vec4 clear_color;

        stack<State> states;
    protected:
        virtual void call(GameInstance& instance) = 0;
    public:
        RenderTask(GameInstance& game, const string& identifier_in, Canvas* canvas_in = nullptr);
        RenderTask(GameInstance& game, const string& identifier_in, const PropertySet& uniform_in, Canvas* canvas_in = nullptr);
        RenderTask(GameInstance& game, const string& identifier_in, const PropertySet& attrib_in, const PropertySet& uniform_in, Canvas* canvas_in = nullptr);
        virtual ~RenderTask();

        const string& getIdentifier() const;
        virtual void render(GameInstance& instance);
        State& getState();
        const State& getState() const;
        void pushState();
        void popState();
        const PropertySet& getAttribs() const;
        const PropertySet& getUniforms() const;
        PropertySet constructDefaultAttrib() const;
        PropertySet constructDefaultUniforms() const;
        void setClearColor(const Vec4& v);
        const Vec4& getClearColor() const;
    };
};

#endif