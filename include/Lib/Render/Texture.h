#ifndef MAGE_RENDER_TEXTURE_H
#define MAGE_RENDER_TEXTURE_H

#include "Definitions.h"
#include "Lib/Math/Vectors.h"
#include "glad/glad.h"
#include "SOIL/SOIL.h"
#include <string>

namespace mage {
    using namespace std;

    class Texture {
        GLuint id;
        int width, height;
    
    protected:
        void createFromData(const GLubyte* data);
        void useDefaultFlags();
    public:
        Texture(const string& path);
        Texture(int width_in, int height_in);
        Texture(int width_in, int height_in, const Vec4& color);
        ~Texture();
        Texture(const Texture& other);
        Texture& operator=(const Texture& other);

        GLuint getId() const;
        int getWidth() const;
        int getHeight() const;
    };
}

#endif