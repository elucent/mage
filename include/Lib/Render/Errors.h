#ifndef MAGE_RENDER_ERRORS_H
#define MAGE_RENDER_ERRORS_H

#include "Definitions.h"
#include "glad/glad.h"
#include <string>

namespace mage {
    using namespace std;

    class ShaderError : public exception {
        string message;
    public:
        ShaderError(const string& message_in, GLuint shader_type, GLuint shader);

        virtual const char* what() const noexcept override;
    };

    class ProgramError : public exception {
        string message;
    public:
        ProgramError(const string& message_in, GLuint program);

        virtual const char* what() const noexcept override;
    };

    class StateError : public exception {
        string message;
    public:
        StateError(const string& message_in);

        virtual const char* what() const noexcept override;
    };

    class DrawError : public exception {
        string message;
    public:
        DrawError(const string& message_in);

        virtual const char* what() const noexcept override;
    };
}

#endif