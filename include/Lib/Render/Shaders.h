#ifndef MAGE_RENDER_SHADER_H
#define MAGE_RENDER_SHADER_H

#include "Definitions.h"
#include "Lib/Game/Management.h"
#include "Lib/Utility/Strings.h"
#include "Lib/Render/Errors.h"
#include "Lib/Render/State.h"
#include "glad/glad.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <fstream>

namespace mage {
    using namespace std;

    class Shader {
        GLuint id;
        string name;
        PropertySet attributes;
        PropertySet uniforms;

        unordered_map<string, GLuint> attrib_locations;
        unordered_map<string, GLuint> uniform_locations;
    public:
        Shader(RenderManager& game, const string& name_in, const string& vsh, const string& fsh);
        ~Shader();
        Shader(const Shader& other) = delete;
        Shader& operator=(const Shader& other) = delete;

        const PropertySet& getAttribs() const;
        const PropertySet& getUniforms() const;
        int getAttribLocation(const string& attrib) const;
        int getUniformLocation(const string& uniform) const;
        GLuint getId() const;
        const string& getName() const;
    };
}

#endif