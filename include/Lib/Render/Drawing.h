#ifndef MAGE_RENDER_DRAWING_H
#define MAGE_RENDER_DRAWING_H

#include "Definitions.h"
#include "Lib/Render/Task.h"
#include "glad/glad.h"
#include <vector>

namespace mage {
    using namespace std;

    class Shape {
    public:
        virtual void addToDrawing(Drawing& drawing) const = 0;
    };

    class Drawing {
        vector<vector<float>> data;
        vector<GLuint> buffers;
        vector<GLuint> indices;
        GLuint elements;
        PropertyType position_type, color_type, normal_type, texcoord_type, sprite_type;
        bool dirty;
        int vertex_count;
        void put(GLuint attrib, float f);
    public:
        const static constexpr GLuint POSITION_INDEX = 0;
        const static constexpr GLuint COLOR_INDEX = 1;
        const static constexpr GLuint NORMAL_INDEX = 2;
        const static constexpr GLuint TEXCOORD_INDEX = 3;
        const static constexpr GLuint SPRITE_INDEX = 4;

        Drawing(const RenderTask& task);
        Drawing(GameInstance& game);
        ~Drawing();

        void clear();
        void draw(const RenderTask& task);
        void draw(GameInstance& game);

        GLuint getVertexCount() const;

        Drawing& addPosition(float x, float y, float z);
        Drawing& addPosition(float x, float y);
        Drawing& addPosition(Vec3 xyz);
        Drawing& addPosition(Vec2 xy);
        Drawing& addColor(float r, float g, float b, float a);
        Drawing& addColor(float r, float g, float b);
        Drawing& addColor(Vec4 rgba);
        Drawing& addColor(Vec3 rgb);
        Drawing& addNormal(float x, float y, float z);
        Drawing& addNormal(Vec3 xyz);
        Drawing& addTexcoord(float u, float v);
        Drawing& addTexcoord(Vec2 uv);
        Drawing& addSprite(float x, float y, float w, float h);
        Drawing& addSprite(const Sprite& sprite);

        Drawing& traceVertex(GLuint i, GLuint base = 0, bool newvert = true);
        Drawing& traceTriangle(GLuint a, GLuint b, GLuint c, GLuint base = 0);
        Drawing& traceQuad(GLuint a, GLuint b, GLuint c, GLuint d, GLuint base = 0);

        Drawing& addShape(const Shape& shape);
        Drawing& addDrawing(const Drawing& drawing);
    };

    class ShapePropertied : public Shape {
    protected:
        Vec4 color;
        Vec4 texcoords;
        Sprite sprite;
        bool lit;
    public:
        ShapePropertied();
    
        ShapePropertied& setColor(float r, float g, float b);
        ShapePropertied& setColor(float r, float g, float b, float a);
        ShapePropertied& setColor(const Vec3& color_in);
        ShapePropertied& setColor(const Vec4& color_in);

        ShapePropertied& setTexcoords(float u, float v, float w, float h);
        ShapePropertied& setTexcoords(const Vec4& texcoords_in);

        ShapePropertied& setSprite(float x, float y, float w, float h);
        ShapePropertied& setSprite(const Sprite& sprite_in);

        ShapePropertied& setUnlit();
        ShapePropertied& setLit();

        virtual void addToDrawing(Drawing& drawing) const = 0;
    };

    class ShapeSquare : public ShapePropertied {
        Vec3 upperleft, lowerright;
        Vec3 normal;
    public:
        ShapeSquare(const Vec3& upperleft_in, const Vec3& lowerright_in);

        virtual void addToDrawing(Drawing& drawing) const;
    };

    class ShapeCircle : public ShapePropertied {
        Vec3 upperleft, lowerright;
        Vec3 normal;
        int steps;
    public:
        ShapeCircle(const Vec3& upperleft_in, const Vec3& lowerright_in, int steps_in = 16);

        virtual void addToDrawing(Drawing& drawing) const;
    };

    class ShapeTriangle : public ShapePropertied {
        Vec3 a, b, c;
        Vec3 normal;
    public:
        ShapeTriangle(const Vec3& a_in, const Vec3& b_in, const Vec3& c_in);

        virtual void addToDrawing(Drawing& drawing) const;
    };

    class ShapeQuad : public ShapePropertied {
        Vec3 a, b, c, d;
        Vec3 normal;
    public:
        ShapeQuad(const Vec3& a_in, const Vec3& b_in, const Vec3& c_in, const Vec3& d_in);

        virtual void addToDrawing(Drawing& drawing) const;
    };

    class ShapeCube : public ShapePropertied {
        Vec3 upperleft, lowerright;
        Sprite sprites[6];
        Vec4 facecoords[6];
    public:
        ShapeCube(const Vec3& upperleft_in, const Vec3& lowerright_in, bool flat_in = false);
        ShapeCube(const BoundingBox& box, bool flat_in = false);

        ShapePropertied& setSprite(const Sprite& spr) = delete;
        ShapeCube& setLeft(const Sprite& spr);
        ShapeCube& setRight(const Sprite& spr);
        ShapeCube& setTop(const Sprite& spr);
        ShapeCube& setBottom(const Sprite& spr);
        ShapeCube& setBack(const Sprite& spr);
        ShapeCube& setFront(const Sprite& spr);
        ShapeCube& setAll(const Sprite& spr);
        ShapeCube& autoTexcoords();
        virtual void addToDrawing(Drawing& drawing) const;
    };

    class ShapeCylinder : public ShapePropertied {
        Vec3 upperleft, lowerright;
        int steps;
    public:
        ShapeCylinder(const Vec3& upperleft_in, const Vec3& lowerright_in, int steps_in = 16);

        virtual void addToDrawing(Drawing& drawing) const;
    };
}

#endif