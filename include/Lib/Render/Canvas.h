#ifndef MAGE_RENDER_CANVAS_H
#define MAGE_RENDER_CANVAS_H

#include "Definitions.h"
#include "Lib/Render/Texture.h"
#include "glad/glad.h"

namespace mage {
    using namespace std;

    class Canvas {
        string name;
        Texture texture;
        GLuint framebuffer, renderbuffer;
    public:
        Canvas(const string& name_in, int width_in, int height_in);
        ~Canvas();
        Canvas(const Canvas& other) = delete;
        Canvas& operator=(const Canvas& other) = delete;

        const string& getName() const;
        int getWidth() const;
        int getHeight() const;
        const Texture* getTexture() const;
        void bind() const;
        static void unbind();
        void resize(int width_in, int height_in);
    };
}

#endif