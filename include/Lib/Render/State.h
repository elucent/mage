#ifndef MAGE_RENDER_STATE_H
#define MAGE_RENDER_STATE_H

#include "Definitions.h"
#include "Lib/Math/Vectors.h"
#include "Lib/Math/Matrix.h"
#include "Lib/Utility/Memory.h"
#include <unordered_map>

namespace mage {
    using namespace std;

    bool IsPropertyType(int gltype);
    PropertyType GetPropertyType(int gltype);
    int GetPropertySize(PropertyType type);
    ostream& operator<<(ostream& os, PropertyType type);

    class PropertySet {
        unordered_map<string, PropertyType> properties;
    public:
        PropertySet();

        PropertySet& addProperty(const string& name, PropertyType type);
        bool hasMember(const string& name) const;
        PropertyType getMemberType(const string& name) const;
        bool isSubsetOf(const PropertySet& other) const;
        unordered_map<string, PropertyType>::const_iterator begin() const;
        unordered_map<string, PropertyType>::const_iterator end() const;

        friend class UniformLayout;
    };

    class UniformLayout {
        unordered_map<string, int> layout;
        int size;
        const PropertySet* properties;
    public:
        UniformLayout(const PropertySet& properties_in);

        PropertyType getUniformType(const string& name) const;
        const PropertySet& getProperties() const;
        int getUniformPosition(const string& name) const;
        int getSize() const;
    };

    class State {
        Shader* active;
        const UniformLayout* layout;
        vector<unsigned char> data;
        vector<const Texture*> boundtextures;

        void checkUniformValidity(const string& uniform, PropertyType type) const;
        void apply(const string& uniform) const;
    public:
        State(const UniformLayout* layout_in);

        void apply() const;

        Shader* getShader();
        const Shader* getShader() const;
        void setShader(Shader* shader);
        void setInteger(const string& name, int value);
        void setFloat(const string& name, float value);
        void setVector(const string& name, const Vec2& vector);
        void setVector(const string& name, const Vec3& vector);
        void setVector(const string& name, const Vec4& vector);
        void setMatrix(const string& name, const Mat4& matrix);
        void setTexture(const string& name, const Texture* texture, int texpos);
    };
}

namespace std {
    template<>
    struct hash<mage::PropertyType> {
        size_t operator()(const mage::PropertyType& type) const {
            return hash<int>{}((int)type);
        }
    };
}

#endif