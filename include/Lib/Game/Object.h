#ifndef MAGE_GAME_OBJECT_H
#define MAGE_GAME_OBJECT_H

#include "Definitions.h"
#include "Lib/Math/Boundaries.h"
#include "Lib/Math/Functions.h"
#include "Lib/Math/Vectors.h"
#include "Lib/Render/Drawing.h"
#include "event/Event.h"
#include <utility>
#include <unordered_map>
#include <functional>

namespace mage {
    using namespace std;

    class Object {
        friend class World;
        friend class Space;

        World* world;
        Space* space;
        Texture* texture;
        int id;
        bool valid;
        bool alive;
        bool behaves, animated, collides;
    protected:
        BoundingBox box;
        Vec3 motion;
        Vec3 pushout;
        bool wasCollided;
        
        Space* getSpace();
        int getId() const; 
        void validate(Space* space, int id);
    public:
        Object(const BoundingBox& position_in, bool behaves_in = true, bool animated_in = true, bool collides_in = true);
        Object(const BoundingBox& position_in, Texture* texture_in, bool behaves_in = true, bool animated_in = true, bool collides_in = true);
        Object(const Object& other);
        Object& operator=(const Object& other);

        World* getWorld();
        Texture* getBatchTexture();
        void setBatchTexture(Texture* texture_in);
        bool hasBehavior() const;
        bool isCollidable() const;
        bool isAnimated() const;
        virtual void update(GameInstance& game);
        virtual void draw(GameInstance& game);
        virtual void drawInto(GameInstance& game, Drawing& drawing);
        virtual void onCollide(Object* other);
        virtual bool canCollide(Object* other);
        void move(const Vec3& motion);
        void moveTo(const Vec3& position);
        const BoundingBox& getBox() const;
        Vec3 getPosition() const;
        Vec3& getMotion();
        void kill();
        bool isAlive() const;
    };

    class SpaceLocation {
        int x, y, z;
    public:
        SpaceLocation(int x_in, int y_in, int z_in);

        int getX() const;
        int getY() const;
        int getZ() const;
        bool operator==(const SpaceLocation& other) const;
    };
}

namespace std {
    template<>
    struct hash<mage::SpaceLocation> {
        size_t operator()(const mage::SpaceLocation& loc) const {
            hash<int> h{};
            return h(loc.getX()) ^ h(loc.getY()) ^ h(loc.getZ());
        }
    };
}

namespace mage {
    using namespace std;

    class Space {
        friend class World;

        SpaceLocation loc;
        World* world;
        unordered_map<int, Object*> objects;
    public:
        static const constexpr int DIMENSION = 16;

        Space(World* world_in, int x, int y, int z);
        BoundingBox getBounds() const;
        void draw(GameInstance& game);
        void update(GameInstance& game);
        World* getWorld();
    };

    class World : public event::Listener<TickEvent>, public event::Listener<RenderGeometryEvent> {
        unordered_map<SpaceLocation, Space*> spaces;
        unordered_map<int, Object*> objects;
        unordered_set<Object*> tickingObjects;
        unordered_set<Object*> animatedObjects;
        unordered_map<intptr_t, Drawing*> buffers;
        int id;
        bool dirty;
    public:
        World();

        Object* findObject(int id);
        void putObject(Object* object);
        void removeObject(Object* object);
        void removeObjects(const function<bool(Object*)>& predicate);
        Space* findSpace(Vec3 position);
        vector<BoundingBox> getColliders(Object* object);
        vector<BoundingBox> getColliders(const BoundingBox& box);
        void update(GameInstance& game);
        void render(GameInstance& game);
        void rebake();

        virtual void receive(TickEvent& event) override;
        virtual void receive(RenderGeometryEvent& event) override;
    };
}

#endif