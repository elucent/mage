#ifndef MAGE_INPUT_KEYBOARD_H
#define MAGE_INPUT_KEYBOARD_H

#include "Definitions.h"
#include <unordered_set>
#include "GLFW/glfw3.h"

namespace mage {
    using namespace std;

    class Keyboard {
        friend class GameInstance;
        friend void KeyCallback(GLFWwindow*, int, int, int, int);
        static unordered_set<int> tapped;
        static unordered_set<int> pressed;
        static void update(GLFWwindow* window);
    public:
        static bool isKeyDown(int key);
        static bool isKeyTapped(int key);
    };

    class Mouse {
        friend class GameInstance;
        static double x, y;
        static void update(GLFWwindow* window);
    public:
        static double getX();
        static double getY();
    };
}

#endif