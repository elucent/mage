#ifndef MAGE_GAME_MANAGEMENT_H
#define MAGE_GAME_MANAGEMENT_H

#include "Definitions.h"
#include "Lib/Utility/Functions.h"
#include "Lib/Render/Shaders.h"
#include "Lib/Render/State.h"
#include "Lib/Render/Task.h"
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <stack>
#include <vector>

namespace mage {
    using namespace std;

    class SetupInfo {
        string name;
        int width;
        int height;
        int fps;
        bool resizable;
    public:
        SetupInfo();

        SetupInfo& setName(const string& name_in);
        SetupInfo& setWidth(int width_in);
        SetupInfo& setHeight(int height_in);
        SetupInfo& setFPS(int fps_in);
        SetupInfo& setResizable(bool resizable_in);
        const string& getName() const;
        int getWidth() const;
        int getHeight() const;
        int getFPS() const;
        bool isResizable() const;
    };

    class TimeManager {
        double prev;
        int fps;
        double dt;
        double scale;
        double time;
    public:
        TimeManager(int target_fps);

        void update();
        void setScale(double scale_in);
        void setTargetFPS(int target_fps);
        double getUnscaledDelta() const;
        double getDelta() const;
        double getTotal() const;
        double getScale() const;
        int getTargetFPS() const;
    };

    class RenderManager {
        bool valid;
        GLuint vao;
        RenderTask* active;
        unordered_map<string, Shader*> shaders;
        unordered_map<string, Canvas*> canvases;
        vector<RenderTask*> taskqueue;
        unordered_map<string, int> tasks;

        void updateProperties(GameInstance& instance);
    public:
        RenderManager();
        ~RenderManager();

        void removeTask(const string& identifier);
        Shader* addShader(const string& name, const string& vsh, const string& fsh);
        Shader* getShader(const string& name);
        Canvas* addCanvas(const string& name, int width_in, int height_in);
        Canvas* getCanvas(const string& name);
        RenderTask& addTask(RenderTask* task);
        RenderTask& getTask(const string& identifier);
        RenderTask& getActiveTask();
        const RenderTask& getActiveTask() const;
        void init(GameInstance& instance);
        void render(GameInstance& instance);
    };

    class TextureManager {
        unordered_map<string, Texture*> textures;

    public:
        TextureManager();
        ~TextureManager();

        Texture* addTexture(const string& name);
        Texture* getTexture(const string& name);
        const Texture* getTexture(const string& name) const;
    };

    class GameInstance {
        GLFWwindow* handle;
        int width, height;
        string name;
        TimeManager time;
        RenderManager renderer;
        TextureManager textures;
    public:
        GameInstance(const SetupInfo& setup);
        ~GameInstance();
        GameInstance(const GameInstance& other) = delete;
        GameInstance& operator=(const GameInstance& other) = delete;

        int getWidth() const;
        int getHeight() const;
        float getAspect() const;
        TimeManager& getTime();
        RenderManager& getRenderer();
        TextureManager& getTextures();
        void run();
    };

    class TickEvent {
        GameInstance& game;
        double dt;
    public:
        TickEvent(GameInstance& game);

        double getDelta() const;
        GameInstance& getGame();
    };
}

#endif