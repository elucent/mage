#ifndef MAGE_MATH_BOX_H
#define MAGE_MATH_BOX_H

#include "Definitions.h"
#include "Lib/Math/Vectors.h"

namespace mage {
    using namespace std;

    class BoundingBox {
        float x, y, z, w, h, l;
    public:
        BoundingBox(const Vec3& negative_extent, const Vec3& positive_extent);
        BoundingBox(float x_in, float y_in, float z_in, float w_in, float h_in, float l_in);

        float getX() const;
        float getY() const;
        float getZ() const;
        float getWidth() const;
        float getHeight() const;
        float getLength() const;
        float getMinDimension() const;
        Vec3 getCenter() const;
        float volume() const;
        bool intersects(const BoundingBox& box) const;
        Vec3 escape(const BoundingBox& box) const;
        void move(const Vec3& vec);
    };

    class BoundingSphere {
        float x, y, z, radius;
    public:
        BoundingSphere(const Vec3& center_in, float radius_in);
        BoundingSphere(float x_in, float y_in, float z_in, float radius_in);

        float getX() const;
        float getY() const;
        float getZ() const;
        Vec3 getCenter() const;
        float getRadius() const;
        float volume() const;
        bool intersects(const BoundingSphere& sphere) const;
        Vec3 escape(const BoundingSphere& sphere) const;
        void move(const Vec3& vec);
    };
}

#endif