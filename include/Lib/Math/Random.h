#ifndef MAGE_MATH_RANDOM_H
#define MAGE_MATH_RANDOM_H

#include "Definitions.h"
#include "Lib/Math/Functions.h"
#include <random>
#include <limits>

namespace mage {
    using namespace std;

    bool RNGBool();

    int RNGInt();
    int RNGInt(int high);
    int RNGInt(int low, int high);

    float RNGFloat();
    float RNGFloat(float high);
    float RNGFloat(float low, float high);

    double RNGDouble();
    double RNGDouble(double high);
    double RNGDouble(double low, double high);
}

#endif