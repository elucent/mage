#ifndef MAGE_MATH_MATRIX_H
#define MAGE_MATH_MATRIX_H

#include "Definitions.h"
#include "Lib/Math/Errors.h"
#include "Lib/Math/Vectors.h"
#include <utility>

namespace mage {
    template<int R, int C>
    class Mat {
        float data[R * C];
    public:
        Mat();
        Mat(float other);
        Mat(const std::initializer_list<float>& init);
        Mat(const std::initializer_list<std::initializer_list<float>>& init);
        Mat(const std::initializer_list<Vec<C>>& init);
        Mat(const Mat& other);
        Mat& operator=(const Mat& other);

        int width() const;
        int height() const;
        int rows() const;
        int columns() const;
        float& operator()(int row, int column);
        float operator()(int row, int column) const;
        Vec<C> row(int row) const;
        Vec<R> column(int column) const;
        Mat<C, R> transpose() const;
        Mat& operator+=(const Mat& rhs);
        Mat& operator-=(const Mat& rhs);
        Mat& operator*=(const Mat& rhs);
    };

    template<int R, int C>
    Mat<R, C> operator+(const Mat<R, C>& lhs, const Mat<R, C>& rhs);
    template<int R, int C>
    Mat<R, C> operator-(const Mat<R, C>& lhs, const Mat<R, C>& rhs);
    template<int R, int C>
    Mat<R, C> operator-(const Mat<R, C>& operand);
    template<int R1, int C1, int R2, int C2>
    Mat<R1, C2> operator*(const Mat<R1, C1>& lhs, const Mat<R2, C2>& rhs);
    template<int R, int C>
    Vec<R> operator*(const Mat<R, C>& lhs, const Vec<R>& rhs);
    template<int N>
    bool operator==(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    bool operator!=(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int R, int C>
    ostream& operator<<(ostream& os, const Mat<R, C>& operand);
    template<int R, int C>
    istream& operator>>(istream& is, Mat<R, C>& operand);
    
    typedef Mat<2, 2> Mat2;
    typedef Mat<3, 3> Mat3;
    typedef Mat<4, 4> Mat4;

    extern Mat4 Identity();
    extern Mat4 Rotate(float angle, const Vec<3>& axis);
    extern Mat4 Scale(const Vec<3>& scale);
    extern Mat4 Translate(const Vec<3>& translation);
    extern Mat4 Ortho(float left, float right, float top, float bottom, float near, float far);
    extern Mat4 Perspective(float fov, float aspect, float near, float far);
    extern Mat4 Frustum(float left, float right, float top, float bottom, float near, float far);
}

#include "Lib/Math/Matrix.hpp"

#endif