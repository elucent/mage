#include "Lib/Math/Functions.h"

namespace mage {
    using namespace std;

    template<typename ...Args>
    float Min(float lhs, Args... args) {
        return Min(lhs, Min(args...));
    }

    template<typename ...Args>
    float Max(float lhs, Args... args) {
        return Max(lhs, Max(args...));
    }
}