#ifndef __MAGE_MATH_MATRIX_HPP
#define __MAGE_MATH_MATRIX_HPP

#include "Lib/Math/Matrix.h"

namespace mage {
    using namespace std;

    template<int R, int C>
    Mat<R, C>::Mat() {
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) this->operator()(i, j) = 0;
    }

    template<int R, int C>
    Mat<R, C>::Mat(float other) {
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) this->operator()(i, j) = other;
    }
    
    template<int R, int C>
    Mat<R, C>::Mat(const std::initializer_list<float>& init) {
        if (init.size() != R * C) {
            throw DataError(BuildString(
                "Matrix initializer of size ", 
                init.size(), 
                " does not match matrix size ", 
                R, " x ", C,
                "!"
            ));
        }
        int r = 0, c = 0;
        for (float d : init) {
            this->operator()(r, c ++) = d;
            if (c >= C) {
                c = 0;
                r ++;
            }
        }
    }
    
    template<int R, int C>
    Mat<R, C>::Mat(const std::initializer_list<std::initializer_list<float>>& init) {
        int rowsize = init.begin()->size();
        for (auto &row : init) {
            if (row.size() != rowsize) {
                throw DataError(BuildString(
                    "Matrix initializer has uneven column lengths!"
                ));
            }
        }
        if (init.size() != R || rowsize != C) {
            throw DataError(BuildString(
                "Matrix initializer of size ", 
                init.size(), " x ", rowsize,
                " does not match matrix size ", 
                R, " x ", C,
                "!"
            ));
        }
        int r = 0, c = 0;
        for (auto &col : init) {
            c = 0;
            for (float d : col) {
                this->operator()(r, c ++) = d;
            }
            r ++;
        }
    }
    
    template<int R, int C>
    Mat<R, C>::Mat(const std::initializer_list<Vec<C>>& init) {
        if (init.size() != R) {
            throw DataError(BuildString(
                "Matrix initializer of size ", 
                init.size(), " x ", C,
                " does not match matrix size ", 
                R, " x ", C,
                "!"
            ));
        }
        int r = 0, c = 0;
        for (auto &col : init) {
            for (int c = 0; c < C; c ++) {
                this->operator()(r, c) = col[c];
            }
            r ++;
        }
    }
    
    template<int R, int C>
    Mat<R, C>::Mat(const Mat<R, C>& other) {
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) this->operator()(i, j) = other(i, j);
    }
    
    template<int R, int C>
    Mat<R, C>& Mat<R, C>::operator=(const Mat<R, C>& other) {
        if (this != &other) for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) this->operator()(i, j) = other(i, j);
        return *this;
    }
    
    template<int R, int C>
    int Mat<R, C>::width() const {
        return C;
    }
    
    template<int R, int C>
    int Mat<R, C>::height() const {
        return R;
    }
    
    template<int R, int C>
    int Mat<R, C>::rows() const {
        return R;
    }
    
    template<int R, int C>
    int Mat<R, C>::columns() const {
        return C;
    }

    template<int R, int C>
    float& Mat<R, C>::operator()(int row, int column) {
        return data[column * R + row];
    }

    template<int R, int C>
    float Mat<R, C>::operator()(int row, int column) const {
        return data[column * R + row];
    }

    template<int R, int C>
    Vec<C> Mat<R, C>::row(int row) const {
        Vec<C> v;
        for (int i = 0; i < C; i ++) v[i] = this->operator()(row, i);
        return v;
    }
    
    template<int R, int C>
    Vec<R> Mat<R, C>::column(int column) const {
        return this->operator[](column);
    }
    
    template<int R, int C>
    Mat<C, R> Mat<R, C>::transpose() const {
        Mat<C, R> m;
        for (int r = 0; r < R; r ++) {
            for (int c = 0; c < C; c ++) {
                m(c, r) = this->operator()(r, c);
            }
        }
        return m;
    }
    
    template<int R, int C>
    Mat<R, C>& Mat<R, C>::operator+=(const Mat<R, C>& rhs) {
        return *this = *this + rhs;
    }
    
    template<int R, int C>
    Mat<R, C>& Mat<R, C>::operator-=(const Mat<R, C>& rhs) {
        return *this = *this - rhs;
    }
    
    template<int R, int C>
    Mat<R, C>& Mat<R, C>::operator*=(const Mat<R, C>& rhs) {
        return *this = *this * rhs;
    }

    template<int R, int C>
    Mat<R, C> operator+(const Mat<R, C>& lhs, const Mat<R, C>& rhs) {
        Mat<R, C> m;
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) m(i, j) = lhs(i, j) + rhs(i, j);
        return m;
    }
    
    template<int R, int C>
    Mat<R, C> operator-(const Mat<R, C>& lhs, const Mat<R, C>& rhs) {
        Mat<R, C> m;
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) m(i, j) = lhs(i, j) - rhs(i, j);
        return m;
    }
    
    template<int R, int C>
    Mat<R, C> operator-(const Mat<R, C>& operand) {
        Mat<R, C> m;
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) m(i, j) = -operand(i, j);
        return m;
    }
    
    template<int R1, int C1, int R2, int C2>
    Mat<R1, C2> operator*(const Mat<R1, C1>& lhs, const Mat<R2, C2>& rhs) {
        if (C1 != R2) {
            throw ArithmeticError(BuildString(
                "Matrix multiplication between a ",
                R1, " x ", C1,
                " and ",
                R2, " x ", C2, 
                " is impossible!"
            ));
        }

        Mat<R1, C2> m;
        for (int r = 0; r < R1; r ++) {
            for (int c = 0; c < C2; c ++) {
                float sum = 0;
                for (int k = 0; k < C1; k ++){
                    sum += lhs(r, k) * rhs(k, c);
                }
                m(r, c) = sum;
            }
        }
        return m;
    }

    template<int R, int C>
    Vec<R> operator*(const Mat<R, C>& lhs, const Vec<C>& rhs) {
        Vec<R> result;
        for (int r = 0; r < R; r ++) {
            result[r] = 0;
            for (int c = 0; c < C; c ++) {
                result[r] += lhs(r, c) * rhs[c];
            }
        }
        return result;
    }
    
    template<int R, int C>
    bool operator==(const Mat<R, C>& lhs, const Mat<R, C>& rhs) {
        for (int i = 0; i < R; i ++) for (int j = 0; j < C; j ++) if (!Fequal(lhs(i, j),rhs(i, j))) return false;
        return true;
    }
    
    template<int R, int C>
    bool operator!=(const Mat<R, C>& lhs, const Mat<R, C>& rhs) {
        return !(lhs == rhs);
    }
    
    template<int R, int C>
    ostream& operator<<(ostream& os, const Mat<R, C>& operand) {
        os << "[ ";
        for (int r = 0; r < R; r ++) {
            os << "[ ";
            for (int c = 0; c < C; c ++) {
                os << operand(r, c) << " ";
            }
            os << "], ";
        }
        os << "]";
        return os;
    }
    
    template<int R, int C>
    istream& operator>>(istream& is, Mat<R, C>& operand) {
        for (int r = 0; r < R; r ++) for (int c = 0; c < C; c ++) is >> operand(r, c);
        return is;
    }
}

#endif