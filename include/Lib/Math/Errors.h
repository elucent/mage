#ifndef MAGE_MATH_ERRORS_H
#define MAGE_MATH_ERRORS_H

#include "Definitions.h"
#include <string>
#include <exception>

namespace mage {
    using namespace std;

    class ArithmeticError : public exception {
        string message;
    public:
        ArithmeticError(const string& message_in);

        virtual const char* what() const noexcept override;
    };

    class DataError : public exception {
        string message;
    public:
        DataError(const string& message_in);

        virtual const char* what() const noexcept override;
    };
}

#endif