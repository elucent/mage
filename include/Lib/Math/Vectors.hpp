#include "Lib/Math/Vectors.h"

namespace mage {
    using namespace std;

    template<int N>
    Vec<N>::Vec() {
        for (int i = 0; i < N; i ++) data[i] = 0;
    }

    template<int N>
    Vec<N>::Vec(float d) {
        for (int i = 0; i < N; i ++) data[i] = d;
    }

    template<int N>
    Vec<N>::Vec(const std::initializer_list<float>& init) {
        int i = 0;
        for (float d : init) data[i ++] = d;
        if (i != N) {
            throw DataError(BuildString(
                "Vector initializer of size ", 
                init.size(), 
                " does not match vector size ", 
                N, 
                "!"
            ));
        }
    }

    template<int N>
    Vec<N>::Vec(const Vec& other) {
        for (int i = 0; i < N; i ++) data[i] = other[i];
    }

    template<int N>
    Vec<N>& Vec<N>::operator=(const Vec& other) {
        if (this != &other) for (int i = 0; i < N; i ++) data[i] = other[i];
        return *this;
    }
    
    template<int N>
    int Vec<N>::Size() const {
        return N;
    }

    template<int N>
    float& Vec<N>::operator[](int i) {
        return data[i];
    }

    template<int N>
    float Vec<N>::operator[](int i) const {
        return data[i];
    }

    template<int N>
    float Vec<N>::dist() const {
        return Sqrt(distSq());
    }

    template<int N>
    float Vec<N>::distSq() const {
        float sum = 0;
        for (int i = 0; i < N; i ++) sum += data[i] * data[i];
        return sum;
    }

    template<int N>
    bool Vec<N>::isNormal() const {
        return Fequal(distSq(), 1);
    }

    template<int N>
    Vec<N> Vec<N>::normalize() const {
        Vec<N> v;
        float m = dist();
        for (int i = 0; i < N; i ++) v[i] = data[i] / m;
        return v;
    }

    template<int N>
    template<int L, int R>
    Vec<R - L> Vec<N>::slice() const {
        Vec<R - L> v;
        for (int i = L; i < R; i ++) v[i] = data[L + i];
        return v;
    }

    template<int N>
    Vec<N>& Vec<N>::operator+=(const Vec<N>& rhs) {
        return *this = *this + rhs;
    }

    template<int N>
    Vec<N>& Vec<N>::operator-=(const Vec<N>& rhs) {
        return *this = *this - rhs;
    }

    template<int N>
    Vec<N>& Vec<N>::operator*=(const Vec<N>& rhs) {
        return *this = *this * rhs;
    }

    template<int N>
    Vec<N>& Vec<N>::operator/=(const Vec<N>& rhs) {
        return *this = *this / rhs;
    }

    template<int N>
    Vec<N>& Vec<N>::operator%=(const Vec<N>& rhs) {
        return *this = *this % rhs;
    }

    template<int N>
    Vec<N>& Vec<N>::operator*=(float rhs) {
        return *this = *this * rhs;
    }

    template<int N>
    Vec<N>& Vec<N>::operator/=(float rhs) {
        return *this = *this / rhs;
    }
        
    template<int N>
    Vec<N> operator+(const Vec<N>& lhs, const Vec<N>& rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i] + rhs[i];
        return v;
    }

    template<int N>
    Vec<N> operator-(const Vec<N>& lhs, const Vec<N>& rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i] - rhs[i];
        return v;
    }

    template<int N>
    Vec<N> operator*(const Vec<N>& lhs, const Vec<N>& rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i] * rhs[i];
        return v;
    }

    template<int N>
    Vec<N> operator/(const Vec<N>& lhs, const Vec<N>& rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i] / rhs[i];
        return v;
    }

    template<int N>
    Vec<N> operator%(const Vec<N>& lhs, const Vec<N>& rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = Fmod(lhs[i], rhs[i]);
        return v;
    }

    template<int N>
    Vec<N> operator-(const Vec<N>& operand) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = -operand[i];
        return v;
    }

    template<int N>
    Vec<N> operator*(const Vec<N>& lhs, float rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i] * rhs;
        return v;
    }

    template<int N>
    Vec<N> operator/(const Vec<N>& lhs, float rhs) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i] / rhs;
        return v;
    }

    template<int N>
    bool operator==(const Vec<N>& lhs, const Vec<N>& rhs) {
        for (int i = 0; i < N; i ++) if (!Fequal(lhs[i],rhs[i])) return false;
        return true;
    }

    template<int N>
    bool operator!=(const Vec<N>& lhs, const Vec<N>& rhs) {
        return !(lhs == rhs);
    }

    template<int M, int N>
    Vec<M + N> operator&(const Vec<M>& lhs, const Vec<N>& rhs) {
        Vec<M + N> v;
        for (int i = 0; i < M; i ++) v[i] = lhs[i];
        for (int i = 0; i < N; i ++) v[M + i] = rhs[i];
        return v;
    }

    template<int N>
    Vec<N + 1> operator&(const Vec<N>& lhs, float rhs) {
        Vec<N + 1> v;
        for (int i = 0; i < N; i ++) v[i] = lhs[i];
        v[N] = rhs;
        return v;
    }

    template<int N>
    ostream& operator<<(ostream& os, const Vec<N>& operand) {
        os << "{ ";
        for (int i = 0; i < N; i ++) os << operand[i] << " ";
        os << "}";
        return os;
    }

    template<int N>
    istream& operator>>(istream& is, Vec<N>& operand) {
        for (int i = 0; i < N; i ++) is >> operand[i];
        return is;
    }

    template<int N>
    float Dot(const Vec<N>& lhs, const Vec<N>& rhs) {
        float sum = 0;
        for (int i = 0; i < N; i ++) sum += lhs[i] * rhs[i];
        return sum;
    }

    template<int N>
    Vec<N> Cross(const Vec<N>& lhs, const Vec<N>& rhs) {
        if (N != 3) {
            throw ArithmeticError(BuildString(
                "Attempted to take cross product of '",
                lhs, "' and '", rhs,
                "', which are not three-dimensional!"
            ));
        }
        Vec<N> v;
        v[0] = lhs[1] * rhs[2] - lhs[2] * rhs[1];
        v[1] = lhs[2] * rhs[0] - lhs[0] * rhs[2];
        v[2] = lhs[0] * rhs[1] - lhs[1] * rhs[0];
        return v;
    }

    template<int N>
    Vec<N> Abs(const Vec<N>& operand) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = Abs(operand[i]);
        return v;
    }

    template<int N>
    Vec<N> Clamp(const Vec<N>& operand, float min, float max) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = Clamp(operand[i], min, max);
        return v;
    }

    template<int N>
    Vec<N> Fract(const Vec<N>& operand) {
        Vec<N> v;
        for (int i = 0; i < N; i ++) v[i] = Fract(operand[i]);
        return v;
    }

    template<int N>
    float Min(const Vec<N>& operand) {
        float f = operand[0];
        for (int i = 1; i < N; i ++) f = Min(f, operand[i]);
        return f;
    }

    template<int N>
    float Max(const Vec<N>& operand) {
        float f = operand[0];
        for (int i = 1; i < N; i ++) f = Max(f, operand[i]);
        return f;
    }
}