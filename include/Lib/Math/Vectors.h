#ifndef MAGE_MATH_VECTORS_H
#define MAGE_MATH_VECTORS_H

#include "Definitions.h"
#include "Lib/Math/Errors.h"
#include "Lib/Math/Functions.h"
#include "Lib/Utility/Strings.h"
#include <utility>
#include <iostream>

namespace mage {
    using namespace std;

    template<int N>
    class Vec {
        float data[N];
    public:
        Vec();
        Vec(float d);
        Vec(const std::initializer_list<float>& init);
        Vec(const Vec& other);
        Vec& operator=(const Vec& other);

        int Size() const;
        float& operator[](int i);
        float operator[](int i) const;
        float dist() const;
        float distSq() const;
        bool isNormal() const;
        Vec normalize() const;
        template<int L, int R>
        Vec<R - L> slice() const;

        Vec<N>& operator+=(const Vec<N>& rhs);
        Vec<N>& operator-=(const Vec<N>& rhs);
        Vec<N>& operator*=(const Vec<N>& rhs);
        Vec<N>& operator/=(const Vec<N>& rhs);
        Vec<N>& operator%=(const Vec<N>& rhs);
        Vec<N>& operator*=(float rhs);
        Vec<N>& operator/=(float rhs);
    };
        
    template<int N>
    Vec<N> operator+(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N> operator-(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N> operator*(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N> operator/(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N> operator%(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N> operator-(const Vec<N>& operand);
    template<int N>
    Vec<N> operator*(const Vec<N>& lhs, float rhs);
    template<int N>
    Vec<N> operator/(const Vec<N>& lhs, float rhs);
    template<int N>
    bool operator==(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    bool operator!=(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int M, int N>
    Vec<M + N> operator&(const Vec<M>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N + 1> operator&(const Vec<N>& lhs, float rhs);
    template<int N>
    ostream& operator<<(ostream& os, const Vec<N>& operand);
    template<int N>
    istream& operator>>(istream& is, Vec<N>& operand);
    template<int N>
    float Dot(const Vec<N>& lhs, const Vec<N>& rhs);
    template<int N>
    Vec<N> Cross(const Vec<N>& lhs, const Vec<N>& rhs);

    template<int N>
    Vec<N> Abs(const Vec<N>& operand);
    template<int N>
    Vec<N> Clamp(const Vec<N>& operand, float min, float max);
    template<int N>
    Vec<N> Fract(const Vec<N>& operand);
    template<int N>
    float Min(const Vec<N>& operand);
    template<int N>
    float Max(const Vec<N>& operand);

    typedef Vec<2> Vec2;
    typedef Vec<3> Vec3;
    typedef Vec<4> Vec4;
    typedef Vec4 Sprite;

    Vec2 Radial(float angle);
    Vec3 Radial(float pitch, float yaw);
}

#include "Lib/Math/Vectors.hpp"

#endif