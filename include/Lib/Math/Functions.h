#ifndef MAGE_MATH_FUNCTIONS_H
#define MAGE_MATH_FUNCTIONS_H

#include "Definitions.h"
#include <cmath>

namespace mage {
    using namespace std;

    float Abs(float d);
    float Clamp(float d, float l, float h);
    float Fract(float d);
    int Floor(float d);
    int Ceil(float d);
    int Signum(float d);

    float Sqrt(float d);
    float Exp(float d);
    float Pow(float val, float power);

    float Sin(float d);
    float Cos(float d);
    float Tan(float d);
    float Csc(float d);
    float Sec(float d);
    float Cot(float d);
    float Asin(float d);
    float Acos(float d);
    float Atan(float d);
    float Atan(float y, float x);
    float Degrees(float d);
    float Radians(float d);

    float Fmod(float lhs, float rhs);
    bool Fequal(float lhs, float rhs);

    template<typename ...Args>
    float Min(float lhs, Args... args);
    template<typename ...Args>
    float Max(float lhs, Args... args);
    float Min(float lhs, float rhs);
    float Max(float lhs, float rhs);
}

#include "Lib/Math/Functions.hpp"

#endif