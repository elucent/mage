#ifndef MAGE_PHYSICS_HULLS_H
#define MAGE_PHYSICS_HULLS_H

#include "Definitions.h"
#include "Lib/Math/Boundaries.h"
#include "Lib/Math/Vectors.h"

namespace mage {
    using namespace std;

    class Hull {
    public:
        virtual ~Hull();

        virtual Vec3 center() const = 0;
        virtual Vec3 extent(const Vec3& direction) const = 0;
        Vec3 intersects(const Hull& other) const;
    };

    class SphereHull : public Hull {
        BoundingSphere sphere;
    public:
        SphereHull(const BoundingSphere& sphere_in);

        virtual Vec3 center() const override;
        virtual Vec3 extent(const Vec3& direction) const override;
        BoundingSphere& getBox();
        const BoundingSphere& getBox() const;
    };

    class BoxHull : public Hull {
        BoundingBox box;
    public:
        BoxHull(const BoundingBox& box_in);

        virtual Vec3 center() const override;
        virtual Vec3 extent(const Vec3& direction) const override;
        BoundingBox& getBox();
        const BoundingBox& getBox() const;
    };

    class PolygonHull : public Hull {
        vector<Vec3> points;
        Vec3 c;
    public:
        PolygonHull(const vector<Vec3>& points_in);

        virtual Vec3 center() const override;
        virtual Vec3 extent(const Vec3& direction) const override;
        vector<Vec3>& getPoints();
        const vector<Vec3>& getPoints() const;
    };
}

#endif