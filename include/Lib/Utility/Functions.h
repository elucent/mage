#ifndef MAGE_UTILITY_FUNCTIONS_H
#define MAGE_UTILITY_FUNCTIONS_H

#ifdef _WIN32
#include <windows.h>
#define sleep(ms) Sleep(ms)
#else
#include <unistd.h>
#define sleep(ms) usleep(1000*ms)
#endif

namespace mage {
    
}

#endif