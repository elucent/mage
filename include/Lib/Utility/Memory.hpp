#include "Lib/Utility/Memory.h"

namespace mage {
    using namespace std;

    template<typename T>
    T& RawRead(const unsigned char* dataptr) {
        return *((T*)dataptr);
    }

    template<typename T>
    void Buffer::push(const T& t) {
        size_t i = bytes.size();
        bytes.resize(bytes.size() + sizeof(T));
        *((T*)(&bytes[i])) = t;
    }

    template<typename T>
    T Buffer::pull() {
        T t = *((T*)reader);
        reader += sizeof(T);
        return t;
    }
}
