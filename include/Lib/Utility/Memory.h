#ifndef MAGE_UTILITY_MEMORY_H
#define MAGE_UTILITY_MEMORY_H

#include "Definitions.h"
#include <vector>

namespace mage {
    using namespace std;

    template<typename T>
    T& RawRead(const unsigned char* dataptr);

    class Buffer {
        vector<unsigned char> bytes;
        unsigned char* reader;
    public:
        Buffer();
        Buffer(size_t length_in);

        size_t getSize() const;
        bool isEmpty() const;
        void reset();
        void clear();

        template<typename T>
        void push(const T& t);
        template<typename T>
        T pull();
    };
}

#include "Lib/Utility/Memory.hpp"

#endif