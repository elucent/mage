#ifndef MAGE_UTILITY_STRINGS_H
#define MAGE_UTILITY_STRINGS_H

#include "Definitions.h"
#include <string>
#include <sstream>

namespace mage {
    using namespace std;

    template<typename T>
    string AppendString(T item);
    template<typename T, typename ...Args>
    string AppendString(T item, Args... args);
    template<typename ...Args>
    string BuildString(Args... args);
}

#include "Lib/Utility/Strings.hpp"

#endif