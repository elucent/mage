#include "Lib/Utility/Strings.h"

namespace mage {
    using namespace std;

    template<typename T>
    string AppendString(T item) {
        ostringstream os;
        os << item;
        return os.str();
    }

    template<typename T, typename ...Args>
    string AppendString(T item, Args... args) {
        return AppendString(item) + AppendString(args...);
    }

    template<typename ...Args>
    string BuildString(Args... args) {
        return AppendString(args...);
    }
}