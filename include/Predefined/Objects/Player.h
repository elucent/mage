#ifndef MAGE_OBJECTS_PLAYER_H
#define MAGE_OBJECTS_PLAYER_H

#include "Definitions.h"
#include "Lib/Math/Boundaries.h"
#include "Lib/Game/Object.h"
#include "Lib/Render/Task.h"

namespace mage {
    using namespace std;

    class ObjectPlayer : public Object {
        float pitch, yaw;
    public:
        ObjectPlayer(const BoundingBox& box);

        virtual void update(GameInstance& game) override;
        virtual void draw(GameInstance& game) override;
    }; 
}

#endif