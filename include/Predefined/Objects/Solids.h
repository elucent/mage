#ifndef MAGE_OBJECTS_BOX_H
#define MAGE_OBJECTS_BOX_H

#include "Definitions.h"
#include "Lib/Math/Boundaries.h"
#include "Lib/Game/Object.h"
#include "Lib/Render/Drawing.h"

namespace mage {
    using namespace std;

    class ObjectBox : public Object {
        ShapeCube cube;
    public:
        ObjectBox(const BoundingBox& box, Texture* texture);

        ShapeCube& getCube();
        virtual void drawInto(GameInstance& game, Drawing& drawing) override;
    };
}

#endif