#ifndef MAGE_TASKS_PASSTHROUGH_H
#define MAGE_TASKS_PASSTHROUGH_H

#include "Definitions.h"
#include "Lib/Render/Task.h"

namespace mage {
    using namespace std;

    class PassthroughTask : public RenderTask {
    protected:
        virtual void call(GameInstance& game) override;
    public:
        PassthroughTask(GameInstance& game, const string& identifier, Canvas* canvas_in, Shader* shader_in, Canvas* target = nullptr);
    };

    class CombineTask : public RenderTask {
        Canvas *a, *b;
        Shader *sa, *sb;
    protected:
        virtual void call(GameInstance& game) override;
    public:
        CombineTask(GameInstance& game, const string& identifier, Canvas* canvas_a, Shader* shader_a, Canvas* canvas_b, Shader* shader_b, Canvas* target = nullptr);
    };
}

#endif