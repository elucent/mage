#ifndef MAGE_TASKS_GEOMETRY_H
#define MAGE_TASKS_GEOMETRY_H

#include "Definitions.h"
#include "Lib/Math/Vectors.h"
#include "Lib/Math/Matrix.h"
#include "Lib/Render/Task.h"

namespace mage {
    using namespace std;
    
    class Camera {
        Vec3 pos, angles;
        float fov;
    public:
        Camera(const Vec3 src, const Vec3 dest);
        Camera(const Vec3 pos_in, float pitch, float yaw, float roll = 0);
        
        Vec3& getPos();
        const Vec3& getPos() const;
        void setPos(const Vec3& src_in);
        Vec3& getAngles();
        const Vec3& getAngles() const;
        void setAngles(const Vec3& in);
        void lookAt(const Vec3& src_in);
        float getFOV() const;
        void setFOV(float fov_in);
        Mat4 getMatrix() const;
    };

    class RenderGeometryEvent {
        GameInstance& game;
        GeometryTask& task;
    public:
        RenderGeometryEvent(GameInstance& game_in, GeometryTask& task_in);
        
        GameInstance& getGame();
        GeometryTask& getTask();
        State& getState();
    };

    class GeometryTask : public RenderTask {
        Shader* shader;
        vector<Camera> cameras;
    protected:
        virtual void call(GameInstance& game) override;
    public:
        GeometryTask(GameInstance& game, const string& identifier, Shader* shader_in, Canvas* canvas = nullptr);

        Camera& getCamera();
        const Camera& getCamera() const;
    };
}

#endif