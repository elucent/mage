#ifndef MAGE_DEFINITIONS_H
#define MAGE_DEFINITIONS_H

namespace mage {
    /*
     * Definitions.
     * 
     * This header file should contain every global constant
     * and every class declaration in Mage.
     */

    //
    //
    // LIB
    //
    //

    /*
     * UTILITY
     */

    // Utility/Memory.h
    class Buffer;

    /*
     * MATH
     */

    static const constexpr double PI = 3.14159265358979323;
    static const constexpr double E = 2.71828182845904523;
    static const constexpr double SQRT_TWO = 1.41421356237309504;
    static const constexpr double SQRT_THREE = 1.73205080756887729;

    // Math/Boundaries.h
    class BoundingBox;
    class BoundingSphere;

    // Math/Errors.h
    class ArithmeticError;

    // Matrix.h
    template<int R, int C>
    class Mat;

    // Vectors.h
    template<int N>
    class Vec;

    /*
     * GAME
     */

    // Game/Input.h

    // Game/Management.h
    class SetupInfo;
    class TimeManager;
    class RenderManager;
    class GameInstance;
    class TickEvent;

    // Game/Object.h
    class Object;
    class Space;
    class World;

    /*
     * PHYSICS
     */

    // Physics/Hulls.h
    class Hull;
    class SphereHull;
    class BoxHull;
    class PolygonHull;

    /*
     * RENDER
     */

    const char* const POSITION_ATTRIB_NAME = "MAGE_position";
    const char* const COLOR_ATTRIB_NAME = "MAGE_color";
    const char* const NORMAL_ATTRIB_NAME = "MAGE_normal";
    const char* const TEXCOORD_ATTRIB_NAME = "MAGE_texcoord";
    const char* const SPRITE_ATTRIB_NAME = "MAGE_sprite";

    // Render/Drawings.h
    class Shape;
    class Triangle;
    class Rectangle;
    class Drawing;

    // Render/Errors.h
    class ShaderError;
    class ProgramError;

    // Render/Task.h
    class RenderTask;

    // Render/Shader.h  
    class Shader;
    class PropertySet;
    class ShaderGroup;

    // Render/Texture.h
    class Texture;

    // Render/Canvas.h
    class Canvas;

    // Render/State.h
    enum class PropertyType {
        NONE, 
        INT,
        FLOAT,
        VECTOR2,
        VECTOR3,
        VECTOR4,
        MATRIX4,
        TEXTURE
    };
    class PropertySet;
    class UniformLayout;
    class State;

    //
    //
    // PREDEFINED
    //
    //

    /*
     * Objects
     */

    // Objects/Player.h
    class ObjectPlayer;

    // Objects/Solids.h
    class ObjectBox;

    //
    //
    // RENDERING
    //
    //

    /*
     * TASKS
     */

    // Tasks/Geometry.h
    class RenderGeometryEvent;
    class GeometryTask;

    // Tasks/Passthrough.h
    class PassthroughTask;
}

#endif