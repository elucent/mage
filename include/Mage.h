#ifndef MAGE_H
#define MAGE_H

#include "Definitions.h"

#include "glad/glad.h"
#include "GLFW/glfw3.h"

// Lib

#include "Lib/Game/Input.h"
#include "Lib/Game/Management.h"
#include "Lib/Game/Object.h"

#include "Lib/Math/Boundaries.h"
#include "Lib/Math/Errors.h"
#include "Lib/Math/Functions.h"
#include "Lib/Math/Matrix.h"
#include "Lib/Math/Random.h"
#include "Lib/Math/Vectors.h"

#include "Lib/Physics/Hulls.h"

#include "Lib/Render/Canvas.h"
#include "Lib/Render/Drawing.h"
#include "Lib/Render/Errors.h"
#include "Lib/Render/Shaders.h"
#include "Lib/Render/State.h"
#include "Lib/Render/Task.h"
#include "Lib/Render/Texture.h"

#include "Lib/Utility/Functions.h"
#include "Lib/Utility/Memory.h"
#include "Lib/Utility/Strings.h"

// Predefined

#include "Predefined/Objects/Player.h"
#include "Predefined/Objects/Solids.h"

// Rendering

#include "Rendering/Tasks/Geometry.h"
#include "Rendering/Tasks/Passthrough.h"

#endif