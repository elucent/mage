#ifndef __MAGE_TESTING_H
#define __MAGE_TESTING_H

#include <string>
#include <map>
#include <iostream>
#include <cmath>

#define TEST(fn) void fn (const string& name); \
                   auto fn##_inserted = (mage::__TEST_FUNCS[#fn] = fn); \
                   void fn (const string& name) 

#define RUN() bool __test_result = mage::__TEST_ALL(); \
              int main() { return __test_result ? 0 : 1; }

#define ASSERT_TRUE(a) _ASSERT_TRUE((a))
#define ASSERT_FALSE(a) _ASSERT_FALSE((a))
#define ASSERT_EQUAL(a, b) _ASSERT_EQUAL((a), (b))
#define ASSERT_ALMOST_EQUAL(a, b) _ASSERT_ALMOST_EQUAL((a), (b))
#define FAIL(a) _FAIL((a));

#define _ASSERT_TRUE(a) if (!(a)) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' is false!" << endl; \
                        mage::__TEST_RESULTS[name] = false; \
                        return; \
                    }

#define _ASSERT_FALSE(a) if (a) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' is true!" << endl; \
                        mage::__TEST_RESULTS[name] = false; \
                        return; \
                    }

#define _ASSERT_EQUAL(a, b) if ((a) != (b)) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' != '" << b << "'!" << endl; \
                        mage::__TEST_RESULTS[name] = false; \
                        return; \
                    }

#define _ASSERT_ALMOST_EQUAL(a, b) if (((a)-(b) < 0 ? (b)-(a) : (a)-(b)) > 0.000001) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' != '" << b << "'!" << endl; \
                        mage::__TEST_RESULTS[name] = false; \
                        return; \
                    }

#define _FAIL(a) std::cout << "[FAILURE] Line " << __LINE__ << ": " << a << endl; \
                 mage::__TEST_RESULTS[name] = false; \
                 return;

namespace mage {
    using namespace std;
    using __TESTFN = void (*)(const string&);

    extern map<string, __TESTFN> __TEST_FUNCS;
    extern map<string, bool> __TEST_RESULTS;

    bool __TEST_ALL();
}

#endif